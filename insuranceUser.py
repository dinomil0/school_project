import shelve
from flask import session

class insurancePolicies:
    num = 10000

    def __init__(self, insuranceCompany, policyName, policyNumber, policyType, peopleInsured, amountInsured, paymentPeriod, monthlyPayment, excess, startDate, user):
        # self.__nric = nric
        # self.__accountID = accountID
        self.__class__.num += 1
        self.__productNum = str(self.__class__.num) + "SL"
        self.__insuranceCompany = insuranceCompany
        self.__policyName = policyName
        self.__policyNumber = policyNumber
        self.__policyType = policyType
        self.__peopleInsured = peopleInsured
        self.__amountInsured = amountInsured
        self.__paymentPeriod = paymentPeriod
        self.__monthlyPayment = monthlyPayment
        self.__excess = excess
        self.__startDate = startDate
        self.__endDate = ""
        self.set_endDate()
        self.__user = user
        self.__session = ""

    # def get_nric(self):
    #     return self.__nric
    #
    # def get_accountID(self):
    #     return self.__accountID

    def get_insuranceCompany(self):
        return self.__insuranceCompany

    def get_productNum(self):
        return self.__productNum

    def get_policyName(self):
        return self.__insuranceCompany + " " + self.__policyName

    def get_policyNumber(self):
        return self.__policyNumber

    def get_policyType(self):
        return self.__policyType

    def get_peopleInsured(self):
        return self.__peopleInsured

    def get_amountInsured(self):
        return self.__amountInsured

    def get_monthlyPayment(self):
        return self.__monthlyPayment

    def get_excess(self):
        return self.__excess

    def get_startDate(self):
        return self.__startDate

    def get_endDate(self):
        return self.__endDate

    def get_paymentPeriod(self):
        return self.__paymentPeriod

    def get_user(self):
        return self.__user

    def get_session(self):
        return self.__session

    # def set_nric(self, nric):
    #     self.__nric = nric

    def set_insuranceCompany(self, insuranceCompany):
        self.__insuranceCompany = insuranceCompany

    def set_productNum(self, productNum):
        self.__productNum = productNum

    def set_policyname(self, policyName):
        self.__policyName = policyName

    def set_policyNumber(self, policyNumber):
        self.__policyNumber = policyNumber

    def set_policyType(self, policyType):
        self.__policyType = policyType

    def set_peopleInsured(self, peopleInsured):
        self.__peopleInsured = peopleInsured

    def set_amountInsured(self, amountInsured):
        self.__amountInsured = amountInsured

    def set_monthlyPayment(self, monthlyPayment):
        self.__monthlyPayment = monthlyPayment

    def set_excess(self, excess):
        self.__excess = excess

    def set_startDate(self):
        dateList = []
        startDate = self.__startDate
        dateList = startDate.split("-")
        self.__startDate = dateList
        # self.__startDate = startDate

    def set_endDate(self):
        dateList = []
        startDate = self.__startDate
        dateList = startDate.split("-")
        year = int(dateList[0]) + self.__paymentPeriod
        month = int(dateList[1])
        if month < 10:
            date = int(dateList[2]) + 1
            if date >= 30:
                month = int(dateList[1]) + 1
                date = 1
                self.__endDate = "{}-0{}-{}".format(year, month, date)
            else:
                self.__endDate = "{}-0{}-{}".format(year, month, date)
        else:
            date = int(dateList[2]) + 1
            if date >= 30:
                month = int(dateList[1]) + 1
                date = 1
                self.__endDate = "{}-{}-{}".format(year, month, date)
            else:
                self.__endDate = "{}-{}-{}".format(year, month, date)

    def set_paymentPeriod(self, paymentPeriod):
        self.__paymentPeriod = paymentPeriod

    def set_user(self, user):
        self.__user = user


i1 = insurancePolicies("Prudential", "PruShield", "1234A", "Health", "Parents", 300000, 10, 187.60, 0, "2019-01-01", 1)
i2 = insurancePolicies("AIA", "Integrated Shield", "2345B", "Accident", "Family", 250000, 10, 91.50, 0, "2017-03-25", 1)
i3 = insurancePolicies("NTUC Income", "VivoLife", "3456C", "Life", "Spouses", 300000, 30, 220.50, 0, "2018-12-20", 1)
i4 = insurancePolicies("NTUC Income", "Enhanced Home Insurance", "4567D", "House", "House", 620000, 2, 70, 0, "2018-07-15", 1)

i5 = insurancePolicies("FWD", "Car Insurance", "1324A", "Car", "Car", 0, 10, 200, 600, "2015-03-10", 2)
i6 = insurancePolicies("AIA", "Pro Lifetime Protector", "2435B", "Life", "Parents", 450000, 5, 165, 0, "2017-07-29", 2)
i7 = insurancePolicies("NTUC Income", "IncomeShield", "3546C", "Health", "Parents", 500000, 5, 170, 0, "2016-06-13", 2)
i8 = insurancePolicies("Prudential", "PRUmortgage", "4657D", "House", "House", 700000, 25, 90.3, 0, "2015-01-20", 2)
i9 = insurancePolicies("FWD", "Travel Insurance", "5768E", "Travel", "Myself", 500000, 5, 100, 0, "2018-01-05", 2)

i10 = insurancePolicies("FWD", "Travel Insurance", "1432A", "Travel", "Myself", 500000, 5, 100, 0, "2018-01-21", 3)
i11 = insurancePolicies("FWD", "Car Insurance", "2543B", "Car", "Car", 0, 10, 200, 600, "2015-06-27", 3)
i12 = insurancePolicies("AIA", "Solitaire Personal Accident", "3654C", "Accident", "Myself", 500000, 5, 100, 0, "2018-01-30", 3)
i13 = insurancePolicies("Prudential", "PRUmortgage", "4765D", "House", "House", 500000, 5, 100, 0, "2018-01-04", 3)
i14 = insurancePolicies("Prudential", "PRUextra", "5876E", "Health", "Parents", 500000, 5, 100, 0, "2018-01-15", 3)
i15 = insurancePolicies("AIA", "Pro Lifetime Protector", "6987F", "Life", "Myself", 500000, 5, 100, 0, "2018-01-12", 3)

db = shelve.open('insurance.db', 'w')
insuranceList = {}
insuranceList = db['Accounts']
insuranceList[i1.get_policyNumber()] = i1
insuranceList[i2.get_policyNumber()] = i2
insuranceList[i3.get_policyNumber()] = i3
insuranceList[i4.get_policyNumber()] = i4
insuranceList[i5.get_policyNumber()] = i5
insuranceList[i6.get_policyNumber()] = i6
insuranceList[i7.get_policyNumber()] = i7
insuranceList[i8.get_policyNumber()] = i8
insuranceList[i9.get_policyNumber()] = i9
insuranceList[i10.get_policyNumber()] = i10
insuranceList[i11.get_policyNumber()] = i11
insuranceList[i12.get_policyNumber()] = i12
insuranceList[i13.get_policyNumber()] = i13
insuranceList[i14.get_policyNumber()] = i14
insuranceList[i15.get_policyNumber()] = i15

db['Accounts'] = insuranceList
db.close()