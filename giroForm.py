class giro:
    def __init__(self, user, bank, accNum, giroName):
        self.__user = user
        self.__bank = bank
        self.__accNum = accNum
        self.__giroName = giroName
        self.__giroNum = ""
        self.__giroType= ""
        self.__giroAmt = ""
        self.__paidStatus = ""

    def get_user(self):
        return self.__user

    def get_bank(self):
        return self.__bank

    def get_accNum(self):
        return self.__accNum

    def get_giroName(self):
        return self.__giroName

    def get_giroNum(self):
        return self.__giroNum

    def get_giroType(self):
        return self.__giroType

    def get_giroAmt(self):
        return self.__giroAmt

    def get_paidStatus(self):
        return self.__paidStatus

    def set_user(self, user):
        self.__user = user

    def set_bank(self, bank):
        self.__bank = bank

    def set_accNum(self, accNum):
        self.__accNum = accNum

    def set_giroName(self, giroName):
        self.__giroName = giroName

    def set_giroNum(self, giroNum):
        self.__giroNum = giroNum

    def set_giroType(self, giroType):
        self.__giroType = giroType

    def set_giroAmt(self, giroAmt):
        self.__giroAmt = giroAmt

    def set_paidStatus(self, status):
        self.__paidStatus = status