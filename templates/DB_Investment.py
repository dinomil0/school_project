import datetime
import StockAPI as API

class Portfolio:
    def __init__(self):
        self.__portfolio = []

    def add_portfolio(self, stock):
        self.__portfolio.append(API.Stocks(stock))

    def get_list(self):
        return self.__portfolio



class Recommendations:
    def __init__(self):
        self.__recommendations = []

    def add_recommendations(self, stock):
        self.__recommendations.append(API.Stocks(stock))

    def get_recommendations(self):
        return self.__recommendations



class Order:
    def __init__(self, stock):
        self.__stock = API.Stocks(stock)
        self.__quantity = ""
        self.__cprice = ""
        self.__ordertype = ""
        self.__total = ""
        self.__status = ""

    def set_buyorder(self, bidprice, quantity):
        self.__cprice = bidprice
        self.__quantity = quantity
        self.__total = self.__cprice * self.__quantity * 100
        self.__ordertype = "buy"

    def set_sellorder(self, askprice, quantity):
        self.__cprice = askprice
        self.__quantity = quantity
        self.__total = self.__cprice * self.__quantity * 100
        self.__ordertype = "sell"

    def get_ordertype(self):
        return self.__ordertype

    def get_total(self):
        return self.__total

    def get_status(self):
        return self.__status

    def validatestatus(self):
        if self.__cprice <= self.__stock.get_askprice() - 3 or self.__cprice >= self.__stock.get_bidprice() + 3:
            self.__status = "unsuccessful"
        else:
            self.__status = "successful"



class TransactionHistory:
    def __init__(self):
        self.__transactionhistory = {}
        self.__datetime = ""

    def add_history(self, order):
        if order.get_status() == "successful":
            self.__transactionhistory[datetime.datetime.now()] = order
        else:
            pass

    def get_transactionhistory(self):
        return self.__transactionhistory


class Wallet:
    def __init__(self, balance):
        self.__balance = balance

    def orderbalance(self, order):
        if order.get_status() == "successful":
            if order.get_ordertype() == "buy":
                self.__balance -= order.get_total()
            else:
                self.__balance += order.get_total()
        else:
            pass

    def accountbalance(self):
        # codes for account