import shelve

class insuranceProduct:
    num = 0
    def __init__(self, name, company, type):
        self.__class__.num += 1
        self.__productNum = self.__class__.num
        self.__name = name
        self.__company = company
        self.__type = type
        self.__description = ""
        self.__link = ""
        self.__promotion = ""
        self.__promotionLink = ""

    def get_productNum(self):
        return self.__productNum

    def get_name(self):
        return self.__company + " " + self.__name

    def get_company(self):
        return self.__company

    def get_type(self):
        return self.__type

    def get_description(self):
        return self.__description

    def get_promotion(self):
        return self.__promotion

    def get_link(self):
        return self.__link

    def get_promotionLink(self):
        return self.__promotionLink

    def set_productNum(self, productNum):
        self.__productNum = productNum

    def set_name(self, name):
        self.__name = name

    def set_company(self, company):
        self.__company = company

    def set_type(self, type):
        self.__type = type

    def set_description(self, description):
        self.__description = description

    def set_promotion(self, promotion):
        self.__promotion = promotion

    def set_link(self, link):
        self.__link = link

    def set_promotionLink(self, link):
        self.__promotionLink = link

# #AIA
# aAccident = insuranceProduct("Solitaire Personal Accident", "AIA", "Accident")
# aAccident.set_description("For the first 5 years, you can enjoy additional coverage of 5% every time you renew your policy.")
# aAccident.set_link("https://www.aia.com.sg/en/our-products/accident-protection/aia-solitaire-personal-accident.html")
#
# aHealth = insuranceProduct("HealthShield Gold Max", "AIA", "Health")
# aHealth.set_description("With a claim limit of up to $2 million, it is one of the highest in the market.")
# aHealth.set_link("https://www.aia.com.sg/en/our-products/medical-protection/aia-healthshield-gold-max.html")
#
# aLife = insuranceProduct("Pro Lifetime Protector", "AIA", "Life")
# aLife.set_description("AIA Pro Lifetime Protector gives you guaranteed protection for the first 10 years regardless of your investment performance.")
# aLife.set_link("https://www.aia.com.sg/en/our-products/life-protection.html")
#
# aTravel = insuranceProduct("Around The World Plus", "AIA", "Travel")
# aTravel.set_description("Provide reimbursement for your local medical expenses for up to 45 days after your return.")
# aTravel.set_link("https://www.aia.com.sg/en/our-products/travel-lifestyle/aia-around-the-world-plus.html")
#
# #FWD
# fAccident = insuranceProduct("Accident Insurance", "FWD", "Accident")
# fAccident.set_description("We cover your medical expenses for the maximum number of infectious diseases than any other insurer currently in the market.")
# fAccident.set_link("https://www.fwd.com.sg/personal-accident-insurance/")
# fAccident.set_promotion("Use promo code CNY28 to get 28% off")
# fAccident.set_promotionLink("https://www.fwd.com.sg/personal-accident-insurance/online-quote/#/quotation/quickQuestions")
#
# fCar = insuranceProduct("Car Insurance", "FWD", "Car")
# fCar.set_description("We provide one-time payment of up to S$500,000 for surviving children if both parents die or are permanently disabled in a car accident.")
# fCar.set_link("https://www.fwd.com.sg/car-insurance/comprehensive-motor-insurance/")
# fCar.set_promotion("")
# fCar.set_promotionLink("")
#
# fHome = insuranceProduct("Home Insurance", "FWD", "House")
# fHome.set_description("With FWD Home Insurance, personalize your cover from S$31 onwards")
# fHome.set_link("https://www.fwd.com.sg/home-insurance")
# fHome.set_promotion("Use promo code CNY28 to get 28% off")
# fHome.set_promotionLink("https://www.fwd.com.sg/home-insurance/online-quote/ownerType#owner")
#
# fTravel = insuranceProduct("Travel Insurance", "FWD", "Travel")
# fTravel.set_description("We provide unlimited medical evacuation cover if you ever need emergency medical evacuation.")
# fTravel.set_link("https://www.fwd.com.sg/travel-insurance/")
# fTravel.set_promotion("")
# fTravel.set_promotionLink("")
#
# #NTUC
# nAccident = insuranceProduct("Personal Accident Insurance", "NTUC Income", "Accident")
# nAccident.set_description("Children will receive a 40% discount on premiums if at least one parent is insured under the same policy.")
# nAccident.set_link("https://www.income.com.sg/insurance/personal-accident-insurance/pa-assurance")
#
# nCar = insuranceProduct("FlexiMileage", "NTUC Income", "Car")
# nCar.set_description("You will receive a 5% loyalty discount if you have been insuring your vehicle with us for more than three years.")
# nCar.set_link("https://www.income.com.sg/insurance/motor-insurance/car-insurance/fleximileage")
#
# nCar2 = insuranceProduct("Drive Master", "NTUC Income", "Car")
# nCar2.set_description("You may enjoy up to 64% premium discount when you renew your policy, simply by being a good driver.")
# nCar2.set_link("https://www.income.com.sg/insurance/motor-insurance/car-insurance/drive-master")
#
# nHealth = insuranceProduct("Enhanced IncomeShield", "NTUC Income", "Health")
# nHealth.set_description("With a Letter of Guarantee, you do not need to pay for hospital deposits")
# nHealth.set_link("https://www.income.com.sg/insurance/health-insurance/enhanced-incomeshield")
#
# nHouse = insuranceProduct("Enhanced Home Insurance", "NTUC Income", "House")
# nHouse.set_description("Protect your home contents, renovations and building against unforeseen events such as fire, theft or flood damages")
# nHouse.set_link("https://www.income.com.sg/insurance/home-lifestyle-insurance/enhanced-home-insurance")
#
# nTravel = insuranceProduct("Travel Insurance", "NTUC Income", "Travel")
# nTravel.set_description("They extend your policy automatically for the first 14 days at no extra charge if you get delayed overseas.")
# nTravel.set_link("https://www.income.com.sg/insurance/travel-insurance")
# nTravel.set_promotion("From 4 January 2019, enjoy 40% discount on single trip Travel Insurance plans!")
# nTravel.set_promotionLink("https://www.income.com.sg/promotions/travel-insurance/travel-promotion#main")
#
# nLife = insuranceProduct("VivoLife", "NTUC Income", "Life")
# nLife.set_description("You can choose your desired premium payment term of 5, 10, 15, 20 or 25 years or up to age 64 or 84 (last birthday).")
# nLife.set_link("https://www.income.com.sg/insurance/life-insurance/whole-life-insurance/vivolife")
#
# #Prudential
# pAccident = insuranceProduct("PRUpersonal accident", "Prudential", "Accident")
# pAccident.set_description("Triple the payout in the event of an accidental injury while travelling on public transport")
# pAccident.set_link("https://www.prudential.com.sg/products/protection/accident/prupersonal-accident#overview")
#
# pHealth = insuranceProduct("PRUextra", "Prudential", "Health")
# pHealth.set_description("Coverage for 365 days of follow-up Traditional Chinese Medicine treatment.")
# pHealth.set_link("https://www.prudential.com.sg/products/medical/pruextra#overview")
#
# pHouse = insuranceProduct("PRUmortgage", "Prudential", "House")
# pHouse.set_description("A range of interest rate from 1% - 7% for selection")
# pHouse.set_link("https://www.prudential.com.sg/products/protection/mortgage/prumortgage")
#
# pLife = insuranceProduct("PRUTerm Vantage", "Prudential", "Life")
# pLife.set_description("For protection against death, terminal illness till 100 years old")
# pLife.set_link("https://www.prudential.com.sg/products/protection/life/pruterm-vantage#overview")
#
# insuranceList = []
# try:
#     db = shelve.open('insurance.db', 'c')
#     try:
#         insuranceList = db['Products']
#     except:
#         pass
#     insuranceList.append(aAccident)
#     insuranceList.append(aHealth)
#     insuranceList.append(aLife)
#     insuranceList.append(aTravel)
#     insuranceList.append(fAccident)
#     insuranceList.append(fCar)
#     insuranceList.append(fHome)
#     insuranceList.append(fTravel)
#     insuranceList.append(nAccident)
#     insuranceList.append(nCar)
#     insuranceList.append(nHealth)
#     insuranceList.append(nHouse)
#     insuranceList.append(nTravel)
#     insuranceList.append(nLife)
#     insuranceList.append(pAccident)
#     insuranceList.append(pHealth)
#     insuranceList.append(pHouse)
#     insuranceList.append(pLife)
#
#     db['Products'] = insuranceList
#     db.close()
# except Exception as e:
#     print(e)
