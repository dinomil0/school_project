import shelve


class Bank:
    def __init__(self, userID, bankName, baccountNum, type):
        self.__userID = userID
        self.__bankName = bankName
        self.__bankAccountNum = baccountNum
        self.__type = type

    # Get Method

    def get_accType(self):
        return self.__type

    def get_userID(self):
        return self.__userID

    def get_bankName(self):
        return self.__bankName

    def get_accountNum(self):
        return self.__bankAccountNum

    # End Of Get

    # Set Method

    # End Of Set


class Savings(Bank):
    def __init__(self, userID, bankName, baccountNum):
        super().__init__(userID, bankName, baccountNum, type="Savings")
        self.__balance = 0

    def get_savingBal(self):
        return self.__balance

    def set_savingBal(self, bal):
        self.__balance = bal

    def deposit_saving(self, bal):
        self.__balance += bal

    def withdraw_saving(self, bal):
        self.__balance -= bal


class Current(Bank):
    def __init__(self, userID, bankName, baccountNum):
        super().__init__(userID, bankName, baccountNum, type="Current")
        self.__balance = 0

    def get_currentBal(self):
        return self.__balance

    def set_currentBal(self, bal):
        self.__balance = bal

    def deposit_current(self, bal):
        self.__balance += bal

    def withdraw_current(self, bal):
        self.__balance -= bal


def static_instance():
    s1 = Savings(1, "DBS", "199-43333-3")
    s1.set_savingBal(7500)
    c1 = Current(1, "DBS", "199-43333-3")
    c1.set_currentBal(5.12)

    s3 = Savings(1, "UOB", "193-45533-6")
    s3.set_savingBal(5000)

    s2 = Savings(2, "UOB", "199-44455-9")
    s2.set_savingBal(540)
    c2 = Current(2, "UOB", "199-44455-9")
    c2.set_currentBal(200)

    s4 = Savings(2, "OCBC", "188-66666-4")
    s4.set_savingBal(100)
    try:
        banks = []
        db = shelve.open('bank.db', 'c')
        banks.extend([s1, c1, s2, c2, s3, s4])
        db['banks'] = banks
        db.close()
    except Exception as e:
        print(e)


static_instance()
