import shelve
import DB_Investment as DBI
from iexfinance.stocks import Stock


def port():
    db = shelve.open('Investment.db', 'c', writeback=True)
    test = DBI.Portfolio(1)
    test1 = DBI.Portfolio(2)
    stock = DBI.Stocks(Stock("AAPL"))
    stock1 = DBI.Stocks(Stock("TSLA"))
    stock2 = DBI.Stocks(Stock("MSFT"))
    stock3 = DBI.Stocks(Stock("AAPL"))
    stock4 = DBI.Stocks(Stock("TSLA"))
    stock5 = DBI.Stocks(Stock("MSFT"))
    test.set_portfolio(stock)
    test.set_portfolio(stock1)
    test.set_portfolio(stock2)
    test1.set_portfolio(stock3)
    test1.set_portfolio(stock4)
    test1.set_portfolio(stock5)
    list = [test, test1]
    db['UserPort'] = list
    portfolio = db['UserPort']
    del db['UserPort']
    investorder = DBI.Order(DBI.Stocks(Stock("AAPL")), 70, 150, 1)
    investorder1 = DBI.Order(DBI.Stocks(Stock("AAPL")), 70, 150, 2)
    investorder2 = DBI.Order(DBI.Stocks(Stock("TSLA")), 50, 300, 1)
    investorder3 = DBI.Order(DBI.Stocks(Stock("TSLA")), 50, 300, 2)
    investorder4 = DBI.Order(DBI.Stocks(Stock("MSFT")), 60, 110, 1)
    investorder5 = DBI.Order(DBI.Stocks(Stock("MSFT")), 60, 110, 2)
    investorder.set_buyorder()
    investorder.set_status(1)
    investorder1.set_buyorder()
    investorder1.set_status(1)
    investorder2.set_buyorder()
    investorder2.set_status(1)
    investorder3.set_buyorder()
    investorder3.set_status(1)
    investorder4.set_buyorder()
    investorder4.set_status(1)
    investorder5.set_buyorder()
    investorder5.set_status(1)
    for i in portfolio:
        if i.get_userID() == investorder.get_userID():
            i.add_portfolio('AAPL', investorder)
            i.add_portfolio('TSLA', investorder2)
            i.add_portfolio('MSFT', investorder4)
            for j in i.get_portfolio():
                j.valuation()
                # print(j.get_value())
        elif i.get_userID() == investorder1.get_userID():
            i.add_portfolio('AAPL', investorder1)
            i.add_portfolio('TSLA', investorder3)
            i.add_portfolio('MSFT', investorder5)
            for j in i.get_portfolio():
                j.valuation()
            #     print(j.get_value())
    db['UserPort'] = portfolio
    db.close()


def hist():
    db = shelve.open('Investment.db', writeback=True)
    test = DBI.TransactionHistory(1)
    test1 = DBI.TransactionHistory(2)
    investorder = DBI.Order(DBI.Stocks(Stock("AAPL")), 10, 160, 1)
    investorder.set_sellorder()
    investorder.set_status(1)
    # investorder.set_number()
    investorder1 = DBI.Order(DBI.Stocks(Stock("TSLA")), 5, 300, 1)
    investorder1.set_sellorder()
    investorder1.set_status(1)
    # investorder1.set_number()
    investorder2 = DBI.Order(DBI.Stocks(Stock("MSFT")), 10, 110, 1)
    investorder2.set_sellorder()
    investorder2.set_status(1)
    # investorder2.set_number()
    investorder3 = DBI.Order(DBI.Stocks(Stock("AAPL")), 10, 160, 1)
    investorder3.set_sellorder()
    investorder3.set_status(1)
    # investorder3.set_number()
    investorder4 = DBI.Order(DBI.Stocks(Stock("TSLA")), 5, 300, 1)
    investorder4.set_sellorder()
    investorder4.set_status(1)
    # investorder4.set_number()
    investorder5 = DBI.Order(DBI.Stocks(Stock("MSFT")), 10, 110, 1)
    investorder5.set_sellorder()
    investorder5.set_status(1)
    # investorder5.set_number()
    test.add_history(investorder)
    test.add_history(investorder1)
    test.add_history(investorder2)
    test1.add_history(investorder3)
    test1.add_history(investorder4)
    test1.add_history(investorder5)
    list = [test, test1]
    db['UserHist'] = list
    db.close()


def risk():
    userID = 1
    userID1 = 2
    userID2 = 3
    db = shelve.open('Investment.db', 'c', writeback=True)
    userrisk = DBI.RiskScore(userID, 'goal', 'aggressive', 'Yes', 'Yes', 'Yes')
    userrisk1 = DBI.RiskScore(userID1, 'goal', 'balanced', 'No', 'Yes', 'No')
    userrisk2 = DBI.RiskScore(userID2, 'general', 'protective', 'No', 'No', 'No')
    userrisk.calculatescore()
    userrisk1.calculatescore()
    userrisk2.calculatescore()
    riskList = [userrisk, userrisk1, userrisk2]
    db['UserRisk'] = riskList
    db.close()


def reco():
    db = shelve.open('Investment.db', 'c', writeback=True)
    userID = 1
    userID1 = 2
    userID2 = 3
    test = DBI.Recommendations(userID)
    test1 = DBI.Recommendations(userID1)
    test2 = DBI.Recommendations(userID2)
    A1 = DBI.Stocks(Stock("TSLA"))
    A2 = DBI.Stocks(Stock("MSFT"))
    A3 = DBI.Stocks(Stock("AAPL"))
    A4 = DBI.Stocks(Stock("FB"))
    A5 = DBI.Stocks(Stock("MU"))
    B1 = DBI.Stocks(Stock("ISRG"))
    B2 = DBI.Stocks(Stock("DVN"))
    B3 = DBI.Stocks(Stock("DXC"))
    B4 = DBI.Stocks(Stock("APTV"))
    B5 = DBI.Stocks(Stock("HUBS"))
    C1 = DBI.Stocks(Stock("GV"))
    C2 = DBI.Stocks(Stock("KRNT"))
    C3 = DBI.Stocks(Stock("DISCA"))
    C4 = DBI.Stocks(Stock("SCHW"))
    C5 = DBI.Stocks(Stock("PDCE"))
    AList = [A1, A2, A3, A4, A5]
    BList = [B1, B2, B3, B4, B5]
    CList = [C1, C2, C3, C4, C5]
    userrisk = db['UserRisk']
    for i in userrisk:
        if i.get_userID() == test.get_userID():
            if 2 <= i.get_score() <= 5:
                test.set_recommendations(CList)
            elif 6 <= i.get_score() <= 9:
                test.set_recommendations(BList)
            else:
                test.set_recommendations(AList)
        elif i.get_userID() == test1.get_userID():
            if 2 <= i.get_score() <= 5:
                test1.set_recommendations(CList)
            elif 6 <= i.get_score() <= 9:
                test1.set_recommendations(BList)
            else:
                test1.set_recommendations(AList)
        elif i.get_userID() == test2.get_userID():
            if 2 <= i.get_score() <= 5:
                test2.set_recommendations(CList)
            elif 6 <= i.get_score() <= 9:
                test2.set_recommendations(BList)
            else:
                test2.set_recommendations(AList)
    list = [test, test1, test2]
    db['UserReco'] = list
    db.close()


def mark():
    db = shelve.open('Investment.db', 'c', writeback=True)
    stocks = []
    stockslist = ["AAPL", "MSFT", "TSLA", "ATVI", "ADBE", "SBUX", "SWIR", "OMCL", "DNKN", "NVDA"]
    for i in stockslist:
        try:
            v = Stock(i)
            stock = DBI.Stocks(v)
            stocks.append(stock)
        except Exception as e:
            print(e)
    market = DBI.Market(stocks)
    db['Market'] = market
    db.close()


def create_auction():
    db = shelve.open('Investment.db', 'c', writeback=True)
    buy = []
    sell = []
    db['buy'] = buy
    db['sell'] = sell
    db.close()

port()
hist()
mark()
create_auction()
risk()
reco()

# buy and sell are list of buy/sell order objects respectively
# buy.append(investorder)
# sell.append(investorder)
# def p2p():
#     db = shelve.open('OOPJ Project/Investment.db', 'w', writeback=True)
#     buy = db['buy']
#     sell = db['sell']
#     print('enter p2p')
#     portfolio = db['UserPort']
#     history = db['UserHist']
#     for s in sell:
#         for b in buy:
#             if s.get_status() == "Unsucessful":
#                 sell.remove(s)
#             if b.get_status() == "Unsucessful":
#                 buy.remove(b)
#             if b.get_userID() != s.get_userID():
#                 if b.get_symbol() == s.get_symbol():
#                     if b.get_price() >= s.get_price():
#                         del db['buy']
#                         del db['sell']
#                         del db['UserPort']
#                         del db['UserHist']
#                         if b.get_quantity() == s.get_quantity():
#                             bOrder = b
#                             sOrder = s
#                             buy.remove(b)
#                             sell.remove(s)
#                             sale = (bOrder.get_price() + sOrder.get_price())/2
#                             # sale is sale price, overrides customer price in display
#                             # sale will take the average of bought price and sell price, only if buy price is higher than
#                             # sell price, allowing buyers to buy at a lower price and sellers to sell at a higher price
#                             # total = bOrder.get_quantity() * sale * 100
#                             # total for both is the same since price and quantity are also the same
#                             bOrder.set_sale(sale)
#                             sOrder.set_sale(sale)
#                             # bOrder.set_total(total)
#                             # sOrder.set_total(total)
#                             bOrder.set_saleorder()
#                             sOrder.set_saleorder()
#                             bOrder.set_status(1)
#                             sOrder.set_status(1)
#                             # status: 0 == unsuccessful, 1 == Successful, 2 == Pending
#                             for i in portfolio:
#                                 print(i.get_userID())
#                                 if i.get_userID() == bOrder.get_userID():
#                                     userbuy = i
#                                     print(userbuy)
#                                 if i.get_userID() == sOrder.get_userID():
#                                     usersell = i
#                                     print(usersell)
#                             index = portfolio.index(userbuy)
#                             index1 = portfolio.index(usersell)
#                             portfolio.remove(userbuy)
#                             portfolio.remove(usersell)
#                             userbuy.add_portfolio(bOrder.get_symbol(), bOrder)
#                             usersell.add_portfolio(sOrder.get_symbol(), sOrder)
#                             portfolio.insert(index, userbuy)
#                             portfolio.insert(index1, usersell)
#                             db['UserPort'] = portfolio
#                             for i in history:
#                                 if i.get_userID() == bOrder.get_userID():
#                                     userhist = i
#                                     index = history.index(i)
#                                     history.remove(i)
#                                     for k in i.get_transactionhistory():
#                                         if k.get_number() == bOrder.get_number():
#                                             userhist.less_history(k)
#                                     userhist.add_history(bOrder)
#                                     history.insert(index, userhist)
#                                 elif i.get_userID() == sOrder.get_userID():
#                                     userhist = i
#                                     index = history.index(i)
#                                     history.remove(i)
#                                     for k in i.get_transactionhistory():
#                                         k.set_sale(sale)
#                                         if k.get_number() == sOrder.get_number():
#                                             userhist.less_history(k)
#                                     userhist.add_history(sOrder)
#                                     history.insert(index, userhist)
#                             db['UserHist'] = history
#                             db['buy'] = buy
#                             db['sell'] = sell
#                             print('a complete')
#                         #     if buy quantity is greater then sell quantity, sell order successful, buy order still pending
#                         elif b.get_quantity() > s.get_quantity():
#                             bOrder = b
#                             sOrder = s
#                             buy.remove(b)
#                             sell.remove(s)
#                             sale = (bOrder.get_total() + sOrder.get_total()) / (bOrder.get_quantity() * 100 + sOrder.get_quantity() * 100)
#                             sOrder.set_status(1)
#                             bOrder.set_status(2)
#                             bOrder.less_quantity(sOrder.get_quantity())
#                             btotal = bOrder.get_quantity() * sale * 100
#                             bOrder.set_total(btotal)
#                             bOrder.set_buyorder()
#                             # original buy order less amt successfully bought
#                             b1 = DBI.Order(DBI.Stocks(Stock(sOrder.get_symbol())), sOrder.get_quantity(), sale, bOrder.get_userID())
#                             b1.set_buyorder()
#                             b1.set_status(1)
#                             b1.set_sale(sale)
#                             b1.set_saleorder()
#                             # create new buy order that is successful as old buy order is partially completed, using same
#                             # symbol, sell order quantity(since it is less than buy order) and sale price therefore no need
#                             # for set_sale(sale)
#                             sOrder.set_sale(sale)
#                             sOrder.set_saleorder()
#
#                             for i in portfolio:
#                                 print(i.get_userID())
#                                 if i.get_userID() == bOrder.get_userID():
#                                     userbuy = i
#                                     print(userbuy)
#                                 if i.get_userID() == sOrder.get_userID():
#                                     usersell = i
#                                     print(usersell)
#                             index = portfolio.index(userbuy)
#                             index1 = portfolio.index(usersell)
#                             portfolio.remove(userbuy)
#                             portfolio.remove(usersell)
#                             userbuy.add_portfolio(b1.get_symbol(), b1)
#                             usersell.add_portfolio(sOrder.get_symbol(), sOrder)
#                             portfolio.insert(index, userbuy)
#                             portfolio.insert(index1, usersell)
#                             db['UserPort'] = portfolio
#                             for i in history:
#                                 if i.get_userID() == bOrder.get_userID():
#                                     userhist = i
#                                     index = history.index(i)
#                                     history.remove(i)
#                                     for k in i.get_transactionhistory():
#                                         if k.get_number() == bOrder.get_number():
#                                             userhist.less_history(k)
#                                     userhist.add_history(b1)
#                                     userhist.add_history(bOrder)
#                                     history.insert(index, userhist)
#                                 elif i.get_userID() == sOrder.get_userID():
#                                     userhist = i
#                                     index = history.index(i)
#                                     history.remove(i)
#                                     for k in i.get_transactionhistory():
#                                         k.set_sale(sale)
#                                         if k.get_number() == sOrder.get_number():
#                                             userhist.less_history(k)
#                                     userhist.add_history(sOrder)
#                                     history.insert(index, userhist)
#                             db['UserHist'] = history
#                             buy.append(bOrder)
#                             db['buy'] = buy
#                             db['sell'] = sell
#                             print('b complete')
#                         #     if sell quantity is greater then buy quantity, buy order successful, sell order still pending
#                         else:
#                             bOrder = b
#                             sOrder = s
#                             buy.remove(b)
#                             sell.remove(s)
#                             sale = (bOrder.get_total() + sOrder.get_total()) / (bOrder.get_quantity() * 100 + sOrder.get_quantity() * 100)
#                             bOrder.set_status(1)
#                             sOrder.set_status(2)
#
#                             sOrder.less_quantity(bOrder.get_quantity())
#                             stotal = sOrder.get_price() * sOrder.get_quantity() * 100
#                             sOrder.set_total(stotal)
#                             sOrder.set_sellorder()
#
#                             s1 = DBI.Order(DBI.Stocks(Stock(bOrder.get_symbol())), bOrder.get_quantity(), sale, sOrder.get_userID())
#                             s1.set_sellorder()
#                             s1.set_sale(sale)
#                             s1.set_status(1)
#                             s1.set_saleorder()
#
#                             bOrder.set_sale(sale)
#                             bOrder.set_saleorder()
#
#                             for i in portfolio:
#                                 print(i.get_userID())
#                                 if i.get_userID() == bOrder.get_userID():
#                                     userbuy = i
#                                     print(userbuy)
#                                 if i.get_userID() == sOrder.get_userID():
#                                     usersell = i
#                                     print(usersell)
#                             index = portfolio.index(userbuy)
#                             index1 = portfolio.index(usersell)
#                             portfolio.remove(userbuy)
#                             portfolio.remove(usersell)
#                             userbuy.add_portfolio(bOrder.get_symbol(), bOrder)
#                             usersell.add_portfolio(s1.get_symbol(), s1)
#                             portfolio.insert(index, userbuy)
#                             portfolio.insert(index1, usersell)
#                             db['UserPort'] = portfolio
#                             for i in history:
#                                 if i.get_userID() == bOrder.get_userID():
#                                     userhist = i
#                                     index = history.index(i)
#                                     history.remove(i)
#                                     for k in i.get_transactionhistory():
#                                         if k.get_number() == bOrder.get_number():
#                                             userhist.less_history(k)
#                                     userhist.add_history(bOrder)
#                                     history.insert(index, userhist)
#                                 elif i.get_userID() == sOrder.get_userID():
#                                     userhist = i
#                                     index = history.index(i)
#                                     history.remove(i)
#                                     for k in i.get_transactionhistory():
#                                         k.set_sale(sale)
#                                         if k.get_number() == sOrder.get_number():
#                                             userhist.less_history(k)
#                                     userhist.add_history(s1)
#                                     userhist.add_history(sOrder)
#                                     history.insert(index, userhist)
#                             db['UserHist'] = history
#                             sell.append(s)
#                             db['buy'] = buy
#                             db['sell'] = sell
#                             print('c complete')
#                         db.close()