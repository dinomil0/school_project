import datetime, time
from iexfinance.stocks import Stock
import shelve

class Stocks:
    def __init__(self, stock):
        self.__name = stock.get_quote()["companyName"]
        self.__symbol = stock.get_quote()["symbol"]
        self.__bidprice = stock.get_price()
        self.__askprice = stock.get_price() - 0.1
        self.__value = 0
        self.__open = stock.get_quote()["open"]
        self.__high = stock.get_quote()["high"]
        self.__low = stock.get_quote()["low"]
        self.__close = stock.get_quote()["close"]
        self.__change = stock.get_quote()["change"]
        self.__changePercent = stock.get_quote()["changePercent"]
        self.__sector = stock.get_quote()["sector"]
        self.__boughtprice = 0
        self.__lot = 0
        self.__total = 0

    def get_name(self):
        return self.__name

    def get_symbol(self):
        return self.__symbol

    def get_bidprice(self):
        return '%.2f' % self.__bidprice

    def get_askprice(self):
        return '%.2f' % self.__askprice

    def get_open(self):
        return self.__open

    def get_high(self):
        return self.__high

    def get_low(self):
        return self.__low

    def get_close(self):
        return self.__close

    def get_change(self):
        return self.__change

    def get_changePercent(self):
        return self.__changePercent

    def get_sector(self):
        return self.__sector

    def get_bprice(self):
        return '%.2f' % self.__boughtprice

    def get_boughtPrice(self):
        return self.__boughtprice

    def get_value(self):
        return '%.2f' % self.__value

    def set_bprice(self):
        if self.__lot == 0:
            pass
        else:
            self.__boughtprice = self.__total/(self.__lot * 100)

    def get_lot(self):
        return self.__lot

    def get_total(self):
        return '%.2f' % self.__total

    def gain_lot(self, new):
        self.__lot += int(new)

    def less_lot(self, new):
        self.__lot -= int(new)

    def gain_total(self, new):
        self.__total = int(self.__total) + int(new)

    def less_total(self, new):
        self.__total -= int(new)

    def valuation(self):
        self.__value = self.__lot * 100 * self.__askprice

    def __str__(self):
        s = "Name: {} \n Symbol: {} \n Bid-Price: {} \n Ask-Price: {}\n Open: {} \n High: {} \n Low: {} \n Close: {} \n Change: {} \n Change(%): {} \n Sector: {} \n Lot: {} \n Total: {}".format(self.get_name(), self.get_symbol(),
                                                                                                                                                                                               self.get_bidprice(), self.get_askprice(),
                                                                                                                                                                                               self.get_open(), self.get_high(),
                                                                                                                                                                                               self.get_low(), self.get_close(),
                                                                                                                                                                                               self.get_change(), self.get_changePercent(),
                                                                                                                                                                                               self.get_sector(), self.get_lot(), self.get_total())
        return s

    def display_stock(self):
        print(self.get_symbol())


class Portfolio:
    def __init__(self, userID):
        self.__portfolio = []
        self.__userID = userID

    def add_portfolio(self, sym, order):
        present = 0
        for p in self.__portfolio:
            if p.get_symbol() == sym:
                if order.get_ordertype() == "buy":
                    p.gain_lot(order.get_quantity())
                    p.gain_total(order.get_total())
                    p.set_bprice()
                else:
                    p.less_lot(order.get_quantity())
                    p.less_total(order.get_total())
                    if p.get_lot() == 0:
                        self.__portfolio.remove(p)
                    p.set_bprice()
                present = 1
            else:
                pass
        if present == 0:
            new = Stocks(Stock(sym))
            if order.get_ordertype() == "buy":
                new.gain_lot(order.get_quantity())
                new.gain_total(order.get_total())
                new.set_bprice()
            else:
                new.less_lot(order.get_quantity())
                new.less_total(order.get_total())
                new.set_bprice()
            self.__portfolio.append(new)

    def get_portfolio(self):
        return self.__portfolio

    def get_userID(self):
        return self.__userID


    def set_portfolio(self, i):
        self.__portfolio.append(i)

    def __str__(self):
        s = "UserID: {}".format(self.__userID)
        return s


class Recommendations:
    def __init__(self, userID):
        self.__recommendations = []
        self.__userID = userID

    def add_recommendations(self, stock):
        self.__recommendations.append(Stocks(stock))

    def get_userID(self):
        return self.__userID

    def get_recommendations(self):
        return self.__recommendations

    def set_recommendations(self, list):
        self.__recommendations.extend(list)


class Order:
    tnumber = 0

    def __init__(self, stock, quantity, cprice, userID):
        self.__userID = userID
        self.__stock = stock
        self.__symbol = stock.get_symbol()
        self.__quantity = quantity
        self.__cprice = cprice
        self.__bprice = cprice
        self.__sprice = cprice
        self.__saleprice = 0
        self.__ordertype = ""
        self.__total = 0
        self.__status = ""
        self.__number = ""
        self.set_number()
        self.__date = ""
        self.__expirydate = time.mktime(datetime.datetime.now().timetuple()) + 86400


    def set_buyorder(self):
        self.__total = self.__cprice * self.__quantity * 100
        self.__ordertype = "buy"
        self.__bprice = self.__total
        self.__sprice = 0

    def set_sellorder(self):
        self.__total = self.__cprice * self.__quantity * 100
        self.__ordertype = "sell"
        self.__sprice = self.__total
        self.__bprice = 0

    def set_saleorder(self):
        self.__total = self.__saleprice * self.__quantity * 100
        if self.__ordertype == "buy":
            self.__bprice = self.__total
        else:
            self.__sprice = self.__total

    def get_userID(self):
        return self.__userID

    def get_symbol(self):
        return self.__symbol

    def get_ordertype(self):
        return self.__ordertype

    def get_quantity(self):
        return self.__quantity

    def get_price(self):
        return self.__cprice

    def get_cprice(self):
        return '%.2f' % self.__cprice

    def get_bprice(self):
        return '%.2f' % float(self.__bprice)

    def get_sprice(self):
        return '%.2f' % float(self.__sprice)

    def get_saleprice(self):
        return '%.2f' % float(self.__saleprice)

    def get_total(self):
        return self.__total

    def get_status(self):
        return self.__status

    def get_date(self):
        return self.__date

    def get_expiry(self):
        if self.__expirydate != "Cleared":
            date = float(self.__expirydate) - time.mktime(datetime.datetime.now().timetuple())
            return str(datetime.timedelta(seconds=date))[0:5]
        else:
            return self.__expirydate

    def get_number(self):
        return self.__number

    def set_date(self, date):
        self.__date = date

    def set_sale(self, price):
        self.__saleprice = price

    def set_total(self, total):
        self.__total = total

    def set_number(self):
        self.__class__.tnumber += 1
        self.__number = self.__symbol + str(self.__class__.tnumber)

    def check_expiry(self):
        if self.__status == "Pending":
            if self.__expirydate <= time.mktime(datetime.datetime.now().timetuple()):
                self.set_status(0)

    def set_status(self, num):
        if num == 1:
            self.__status = "Successful"
            self.__expirydate = "Cleared"
        elif num == 0:
            self.__status = "Unsuccessful"
            self.__expirydate = "Cleared"
        else:
            self.__status = "Pending"

    def less_quantity(self, qty):
        self.__quantity -= qty

    def validatestatus(self, account):
        # if self.__cprice <= self.__stock.get_askprice() - 3 or self.__cprice >= self.__stock.get_bidprice() + 3:
        #     self.__status = "unsuccessful"
        if self.get_total() > account.get_accountBalance():
            self.__status = "Unsuccessful"
        else:
            self.__status = "Pending"

    def __str__(self):
        s = "{} {} {} {} {} {} {} {} {} {} {} {} {}".format(self.__stock.get_name(), self.__quantity, self.__cprice, self.__userID, self.__symbol, self.__bprice, self.__sprice, self.__saleprice, self.__ordertype, self.__total, self.__status, self.__date, self.__expirydate)
        return s

    def float_total(self):
        return '%.2f' % float(self.__total)


class TransactionHistory:
    transactionid = 0
    def __init__(self, userID):
        self.__transactionhistory = []
        self.__userID = userID

    def get_transactionhistory(self):
        return self.__transactionhistory

    def get_userID(self):
        return self.__userID

    def add_history(self, order):
        self.__class__.transactionid += 1
        date = datetime.datetime.now()
        order.set_date(date.strftime("%Y/%m/%d"))
        self.__transactionhistory.append(order)

    def less_history(self, order):
        self.__transactionhistory.remove(order)


class RiskScore:
    def __init__(self, userID, Purpose, Risk, Assessment1, Assessment2, Assessment3):
        self.__userID = userID
        self.purpose = Purpose
        self.risk = Risk
        self.assess1 = Assessment1
        self.assess2 = Assessment2
        self.assess3 = Assessment3
        self.__score = 0

    def calculatescore(self):
        if self.purpose == "goal":
            self.__score += 3
        else:
            self.__score += 1
        if self.risk == "protective":
            self.__score += 1
        elif self.risk == "stable":
            self.__score += 2
        elif self.risk == "balanced":
            self.__score += 3
        elif self.risk == "growth":
            self.__score += 4
        else:
            self.__score += 5
        if self.assess1 == "Yes":
            self.__score += 1
        else:
            self.__score += 0
        if self.assess2 == "Yes":
            self.__score += 3
        else:
            self.__score += 0
        if self.assess3 == "Yes":
            self.__score += 1
        else:
            self.__score += 0

    def get_score(self):
        return self.__score

    def get_userID(self):
        return self.__userID

    # max score == 13
    # lowest score == 2
    # class A == 10-13
    #     class A
    # class B == 6-9
    # class C == 2-5


class Market:
    def __init__(self, market):
        self.__market = market

    def get_market(self):
        return self.__market

    def add_market(self, sym):
        self.__market.append(Stocks(Stock(sym)))

    def remove_market(self, i):
        self.__market.remove(i)

def p2p():
    db = shelve.open('OOPJ Project/Investment.db', 'w', writeback=True)
    buy = db['buy']
    sell = db['sell']
    print('enter p2p')
    portfolio = db['UserPort']
    history = db['UserHist']
    for s in sell:
        for b in buy:
            if s.get_status() == "Unsucessful":
                sell.remove(s)
            if b.get_status() == "Unsucessful":
                buy.remove(b)
            if b.get_userID() != s.get_userID():
                if b.get_symbol() == s.get_symbol():
                    if b.get_price() >= s.get_price():
                        del db['buy']
                        del db['sell']
                        del db['UserPort']
                        del db['UserHist']
                        if b.get_quantity() == s.get_quantity():
                            print("enter a")
                            bOrder = b
                            sOrder = s
                            buy.remove(b)
                            sell.remove(s)
                            sale = (bOrder.get_price() + sOrder.get_price())/2
                            # sale is sale price, overrides customer price in display
                            # sale will take the average of bought price and sell price, only if buy price is higher than
                            # sell price, allowing buyers to buy at a lower price and sellers to sell at a higher price
                            # total = bOrder.get_quantity() * sale * 100
                            # total for both is the same since price and quantity are also the same
                            bOrder.set_sale(sale)
                            sOrder.set_sale(sale)
                            bOrder.set_saleorder()
                            sOrder.set_saleorder()
                            bOrder.set_status(1)
                            sOrder.set_status(1)
                            # status: 0 == unsuccessful, 1 == Successful, 2 == Pending
                            for i in portfolio:
                                print(i.get_userID())
                                if i.get_userID() == bOrder.get_userID():
                                    userbuy = i
                                    print(userbuy)
                                if i.get_userID() == sOrder.get_userID():
                                    usersell = i
                                    print(usersell)
                            index = portfolio.index(userbuy)
                            index1 = portfolio.index(usersell)
                            portfolio.remove(userbuy)
                            portfolio.remove(usersell)
                            userbuy.add_portfolio(bOrder.get_symbol(), bOrder)
                            usersell.add_portfolio(sOrder.get_symbol(), sOrder)
                            for v in userbuy.get_portfolio():
                                v.valuation()
                            for v in usersell.get_portfolio():
                                v.valuation()
                            portfolio.insert(index, userbuy)
                            portfolio.insert(index1, usersell)
                            db['UserPort'] = portfolio
                            for i in history:
                                if i.get_userID() == bOrder.get_userID():
                                    userhist = i
                                    index = history.index(i)
                                    history.remove(i)
                                    for k in i.get_transactionhistory():
                                        if k.get_number() == bOrder.get_number():
                                            userhist.less_history(k)
                                    userhist.add_history(bOrder)
                                    history.insert(index, userhist)
                                elif i.get_userID() == sOrder.get_userID():
                                    userhist = i
                                    index = history.index(i)
                                    history.remove(i)
                                    for k in i.get_transactionhistory():
                                        k.set_sale(sale)
                                        if k.get_number() == sOrder.get_number():
                                            userhist.less_history(k)
                                    userhist.add_history(sOrder)
                                    history.insert(index, userhist)
                            # for i in users:
                            #     if i.get_userID() == bOrder.get_userID():
                            #         user = i
                            #         index = users.index(user)
                            #         users.remove(i)
                            #         i.less_preferredType(total)
                            #         users.insert(index, user)
                            #     elif i.get_userID() == sOrder.get_userID():
                            #         user = i
                            #         index = users.index(user)
                            #         users.remove(i)
                            #         i.add_preferredType(total)
                            #         users.insert(index, user)
                            # dbu['Users'] = users
                            db['UserHist'] = history
                            db['buy'] = buy
                            db['sell'] = sell
                            print('a complete')
                        #     if buy quantity is greater then sell quantity, sell order successful, buy order still pending
                        elif b.get_quantity() > s.get_quantity():
                            print("enter b")
                            bOrder = b
                            sOrder = s
                            buy.remove(b)
                            sell.remove(s)
                            sale = (bOrder.get_total() + sOrder.get_total()) / (bOrder.get_quantity() * 100 + sOrder.get_quantity() * 100)
                            # total = sOrder.get_quantity() * sale * 100
                            sOrder.set_status(1)
                            bOrder.set_status(2)
                            bOrder.less_quantity(sOrder.get_quantity())
                            btotal = bOrder.get_quantity() * sale * 100
                            bOrder.set_total(btotal)
                            bOrder.set_buyorder()
                            # original buy order less amt successfully bought
                            b1 = Order(Stocks(Stock(sOrder.get_symbol())), sOrder.get_quantity(), sale, bOrder.get_userID())
                            b1.set_buyorder()
                            b1.set_status(1)
                            b1.set_sale(sale)
                            b1.set_saleorder()
                            b1.set_number()
                            # create new buy order that is successful as old buy order is partially completed, using same
                            # symbol, sell order quantity(since it is less than buy order) and sale price therefore no need
                            # for set_sale(sale)
                            sOrder.set_sale(sale)
                            sOrder.set_saleorder()

                            for i in portfolio:
                                print(i.get_userID())
                                if i.get_userID() == bOrder.get_userID():
                                    userbuy = i
                                    print(userbuy)
                                if i.get_userID() == sOrder.get_userID():
                                    usersell = i
                                    print(usersell)
                            index = portfolio.index(userbuy)
                            index1 = portfolio.index(usersell)
                            portfolio.remove(userbuy)
                            portfolio.remove(usersell)
                            userbuy.add_portfolio(b1.get_symbol(), b1)
                            usersell.add_portfolio(sOrder.get_symbol(), sOrder)
                            for v in userbuy.get_portfolio():
                                v.valuation()
                            for v in usersell.get_portfolio():
                                v.valuation()
                            portfolio.insert(index, userbuy)
                            portfolio.insert(index1, usersell)
                            db['UserPort'] = portfolio
                            for i in history:
                                if i.get_userID() == bOrder.get_userID():
                                    userhist = i
                                    index = history.index(i)
                                    history.remove(i)
                                    for k in i.get_transactionhistory():
                                        if k.get_number() == bOrder.get_number():
                                            userhist.less_history(k)
                                    userhist.add_history(b1)
                                    userhist.add_history(bOrder)
                                    history.insert(index, userhist)
                                elif i.get_userID() == sOrder.get_userID():
                                    userhist = i
                                    index = history.index(i)
                                    history.remove(i)
                                    for k in i.get_transactionhistory():
                                        k.set_sale(sale)
                                        if k.get_number() == sOrder.get_number():
                                            userhist.less_history(k)
                                    userhist.add_history(sOrder)
                                    history.insert(index, userhist)
                            # for i in users:
                            #     if i.get_userID() == bOrder.get_userID():
                            #         user = i
                            #         index = users.index(user)
                            #         users.remove(i)
                            #         i.less_preferredType(total)
                            #         users.insert(index, user)
                            #     elif i.get_userID() == sOrder.get_userID():
                            #         user = i
                            #         index = users.index(user)
                            #         users.remove(i)
                            #         i.add_preferredType(total)
                            #         users.insert(index, user)
                            # dbu['Users'] = users
                            db['UserHist'] = history
                            buy.append(bOrder)
                            db['buy'] = buy
                            db['sell'] = sell
                            print('b complete')
                        #     if sell quantity is greater then buy quantity, buy order successful, sell order still pending
                        else:
                            print("enter c")
                            bOrder = b
                            sOrder = s
                            buy.remove(b)
                            sell.remove(s)
                            sale = (bOrder.get_total() + sOrder.get_total()) / (bOrder.get_quantity() * 100 + sOrder.get_quantity() * 100)
                            # total = bOrder.get_quantity() * sale * 100
                            bOrder.set_status(1)
                            sOrder.set_status(2)

                            sOrder.less_quantity(bOrder.get_quantity())
                            stotal = sOrder.get_price() * sOrder.get_quantity() * 100
                            sOrder.set_total(stotal)
                            sOrder.set_sellorder()

                            s1 = Order(Stocks(Stock(bOrder.get_symbol())), bOrder.get_quantity(), sale, sOrder.get_userID())
                            s1.set_sellorder()
                            s1.set_sale(sale)
                            s1.set_status(1)
                            s1.set_saleorder()
                            s1.set_number()

                            bOrder.set_sale(sale)
                            bOrder.set_saleorder()

                            for i in portfolio:
                                print(i.get_userID())
                                if i.get_userID() == bOrder.get_userID():
                                    userbuy = i
                                    print(userbuy)
                                if i.get_userID() == sOrder.get_userID():
                                    usersell = i
                                    print(usersell)
                            index = portfolio.index(userbuy)
                            index1 = portfolio.index(usersell)
                            portfolio.remove(userbuy)
                            portfolio.remove(usersell)
                            userbuy.add_portfolio(bOrder.get_symbol(), bOrder)
                            usersell.add_portfolio(s1.get_symbol(), s1)
                            for v in userbuy.get_portfolio():
                                v.valuation()
                            for v in usersell.get_portfolio():
                                v.valuation()
                            portfolio.insert(index, userbuy)
                            portfolio.insert(index1, usersell)
                            db['UserPort'] = portfolio
                            for i in history:
                                if i.get_userID() == bOrder.get_userID():
                                    userhist = i
                                    index = history.index(i)
                                    history.remove(i)
                                    for k in i.get_transactionhistory():
                                        if k.get_number() == bOrder.get_number():
                                            userhist.less_history(k)
                                    userhist.add_history(bOrder)
                                    history.insert(index, userhist)
                                elif i.get_userID() == sOrder.get_userID():
                                    userhist = i
                                    index = history.index(i)
                                    history.remove(i)
                                    for k in i.get_transactionhistory():
                                        k.set_sale(sale)
                                        if k.get_number() == sOrder.get_number():
                                            userhist.less_history(k)
                                    userhist.add_history(s1)
                                    userhist.add_history(sOrder)
                                    history.insert(index, userhist)
                            # for i in users:
                            #     if i.get_userID() == bOrder.get_userID():
                            #         user = i
                            #         index = users.index(user)
                            #         users.remove(i)
                            #         i.less_preferredType(total)
                            #         users.insert(index, user)
                            #     elif i.get_userID() == sOrder.get_userID():
                            #         user = i
                            #         index = users.index(user)
                            #         users.remove(i)
                            #         i.add_preferredType(total)
                            #         users.insert(index, user)
                            # dbu['Users'] = users
                            db['UserHist'] = history
                            sell.append(s)
                            db['buy'] = buy
                            db['sell'] = sell
                            print('c complete')
                        db.close()
