import shelve


class User:
    userID = 0

    def __init__(self, nric, password):
        self.__class__.userID += 1
        self.__accountID = self.__class__.userID
        self.__nric = nric
        self.__password = ""
        self.set_password(password)
        self.__bankType = []
        self.__phoneNum = ""
        self.__preferredType = ""
        self.__notifications = ""

    def get_accountID(self):
        return self.__accountID

    def get_nric(self):
        return self.__nric

    def get_password(self):
        return self.__password

    def get_bankType(self):
        return self.__bankType

    def get_phoneNum(self):
        return self.__phoneNum

    def get_notifications(self):
        return self.__notifications

    def get_preferredType(self):
        return self.__preferredType

    def set_nric(self,nric):
        self.__nric = nric

    def set_password(self,password):
        if len(password) >= 8:
            if password.isalnum():
                self.__password = password

    def set_bankType(self,bankType):
        self.__bankType.append(bankType)

    def set_phoneNum(self,phoneNum):
        self.__phoneNum = phoneNum

    def set_notifications(self, notifications):
        self.__notifications = notifications

    def set_preferredType(self, type):
        self.__preferredType = type

    def add_preferredType(self, amt):
        self.__preferredType.deposit_saving(amt)

    def less_preferredType(self, amt):
        self.__preferredType.withdraw_saving(amt)

    def deposit_saving(self, bal):
        self.balance += bal

    def withdraw_saving(self, bal):
        self.balance -= bal

# u = User(1,"password")
# try:
#     userList = []
#     db = shelve.open('user.db', 'c')
#     # userList = db['Users']
#     userList.append(u)
#     db['Users'] = userList
#     db.close()
#
# except Exception as e:
#     print(e)