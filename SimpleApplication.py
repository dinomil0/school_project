from flask import Flask, render_template, request, make_response, url_for, redirect, session, flash
from wtforms import Form, TextAreaField, SelectField, validators, RadioField, IntegerField, StringField, PasswordField, DateField, SubmitField, FloatField
import secrets
import pygal
from datetime import datetime, timedelta, date, time
from dateutil.relativedelta import relativedelta
from pygal import Config
from pygal.style import LightColorizedStyle, DefaultStyle
from wtforms.fields.html5 import DateField
import shelve
import Bank
import Savings
import db_dashboard
import os
import DB_Investment as DBI
from iexfinance.stocks import Stock
import User
import Account
from datetime import datetime, timedelta
import insuranceUser
from wtforms.validators import DataRequired
import insuranceProduct
import giroForm


app = Flask(__name__)
app.secret_key = os.urandom(16)


# userList = {}
# db = shelve.open('transaction.db', 'c')
# transactions = {}
# transactions[.get_tranID] = objectname


class RequiredIf(object):
    def __init__(self, *args, **kwargs):
        self.conditions = kwargs

    def __call__(self, form, field):
        for name, data in self.conditions.items():
            if name not in form._fields:
                validators.Optional()(field)
            else:
                condition_field = form._fields.get(name)
                if condition_field.data == data:
                    validators.DataRequired().__call__(form, field)
                else:
                    validators.Optional().__call__(form, field)


class newSavings(Form):
    savingType = SelectField('Saving Type', [validators.DataRequired()],
                             choices=[('Personal', 'Personal'), ('Group', 'Group'), ('Other', 'Other')], default="")
    savingGoal = StringField('Saving Goal',
                             [validators.Regexp('^\d*$', message="Must be integer"), validators.DataRequired()])
    savingDeadline = DateField('Deadline', [validators.DataRequired()])
    savingSavers = StringField('Savers', [RequiredIf(savingType != "Personal")])
    savingMethod = RadioField('Deposit Method (Monthly)', [validators.DataRequired()],
                              choices=[('Manually', 'Manually'), ('Auto', 'Auto')])
    savingAmount = StringField('Saving Amount', [
        validators.Regexp('^(?!0*(\.0+)?$)(\d+|\d*\.\d+)$', message="Must be integer and More than zero"),
        RequiredIf(savingMethod == "Auto")])
    bankAccounts = SelectField('Bank Accounts', coerce=str)
    savingDescription = TextAreaField("Description")
    savingSubmit = SubmitField("Submit")


class addBalance(Form):
    savingAmount = StringField('Saving Amount', [
        validators.Regexp('^(?!0*(\.0+)?$)(\d+|\d*\.\d+)$', message="Must be integer and More than zero"),
        validators.DataRequired()])
    bankAccounts = SelectField('Bank Accounts', [RequiredIf(savingAmount != "")], coerce=str)
    savingSubmit = SubmitField("Submit")

# ACCOUNT
class accountForm(Form):
    bank = SelectField("", choices=[('', 'Select')], default='')
# END OF ACCOUNT


@app.route('/')
def home():
    print(session.get('Notification'))
    accForm = accountForm(request.form)
    accountList = {}
    list = []
    try:
        db = shelve.open('account.db', 'r')
        accountList = db['Accounts']
        db.close()
    except Exception as e:
        print(e)

    for account in accountList:
        list.append(accountList[account])
    #Account Details
    try:
        db = shelve.open('bank.db', 'r')
        bankList = db['banks']
        db.close()
    except Exception as e:
        print(e)
    for b in bankList:
        if b.get_userID() == session.get('UserID'):
            if b.get_accType() == "Savings":
                accForm.bank.choices.append((str(b.get_bankName()), str(b.get_bankName())))

    try:
        db = shelve.open('user.db', 'w')
        userList = db['Users']
        if request.method == 'POST':
            for users in userList:
                if userList[users].get_accountID() == session.get('UserID'):
                    userList[users].set_preferredType(accForm.bank.data)
        db['Users'] = userList
        db.close()
    except Exception as e:
        print(e)

    return render_template('Mainpage.html', accountList=list, userList=userList, accountForm=accForm)


# LOGIN
class LoginForm(Form):
    loginUsername = StringField('NRIC', [validators.Regexp('^[stfgSTFG]\d{7}[a-zA-Z]$', message="Invalid NRIC"), validators.DataRequired()])
    loginPassword = PasswordField('Password', [validators.Regexp('^[a-zA-Z]{1,}', message="Invalid Password"), validators.DataRequired(), validators.Length(min=8, max=20)])


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm(request.form)
    if request.method == 'POST' and form.validate():
        userList = {}
        accountList = {}
        try:
            db = shelve.open('user.db', 'r')
            userList = db['Users']
            db.close()
        except:
            print("Fail to open database")

        try:
            db2 = shelve.open('account.db', 'r')
            accountList = db2['Accounts']
            db2.close()
        except:
            print("Fail to open database")


        try:
            for user in userList:
                for account in accountList:
                    if userList[user].get_nric() == form.loginUsername.data:
                        if userList[user].get_password() == form.loginPassword.data:
                            session['UserID'] = userList[user].get_accountID()
                            try:
                                if accountList[account].get_accountID() == userList[user].get_accountID():
                                    session['Name'] = accountList[account].get_name()
                            except:
                                pass
                        else:
                            flash("Invalid Username or Password")
        except Exception as e:
            print(e)

    #Insurance
        x = datetime.now()
        dateList = str(x).split(" ")
        dateList.pop(1)
        for i in dateList:
            dateList = i.split("-")
        dateTrans = dateList[0] + "/" + dateList[1] + "/" + dateList[2]
        date = dateList[-1]
        month = dateList[-2]
        if month == "01":
            month = "13"

        try:
            db = shelve.open('giro.db', 'r')
            gList = db['Forms']
            db.close()
        except Exception as e:
            print(e)
        try:
            db = shelve.open('insurance.db', 'r')
            insuranceList = db['Accounts']
            db.close()
        except:
            print("Fail to open database")

        try:
            db = shelve.open('user.db', 'r')
            userList = db['Users']
            db.close()
        except:
            print("Fail to open database")
        notificationDist = {}
        insuranceNotification = {}
        for insurance in insuranceList:
            if insuranceList[insurance].get_user() == session.get('UserID'):
                insuranceDate = insuranceList[insurance].get_startDate().split('-')
                insuranceDate = insuranceDate[-1]
                if 0 <= (int(insuranceDate) - int(date)) <= 7:
                    for users in userList:
                        if userList[users].get_accountID() == session.get('UserID'):
                            insuranceNotification[insuranceList[insurance].get_policyName()] = int(insuranceDate) - int(date)
                            userList[users].set_notifications(insuranceNotification)
                            notificationDist['Insurance'] = userList[users].get_notifications()
                            session['Notification'] = notificationDist
                elif -23 > (int(insuranceDate) - int(date)) >= -30:
                    for users in userList:
                        if userList[users].get_accountID() == session.get('UserID'):
                            insuranceNotification[insuranceList[insurance].get_policyName()] = int(insuranceDate) + 30 - int(date) + 1
                            userList[users].set_notifications(insuranceNotification)
                            notificationDist['Insurance'] = userList[users].get_notifications()
                            session['Notification'] = notificationDist
        sess = session.get('Notification')
        sess = session.get('Notification').get('Insurance')
        try:
            if len(gList) == 0:
                pass
            else:
                for giro in gList:
                    for insurance in sess:
                        if giro.get_giroName() == insurance:
                            sess.pop(insurance)
        except Exception as e:
            print(e)


    #GIRO
        try:
            db = shelve.open('giro.db', 'r')
            giroList = db['Forms']
            db.close()
        except Exception as e:
            print(e)
        try:
            db = shelve.open('bank.db', 'w')
            bankList = db['banks']
        except Exception as e:
            print(e)
        try:
            db2 = shelve.open('transaction.db', 'w')
            transactionList = db2['transactions']
        except Exception as e:
            print(e)


        bankName = []
        giroNoti = {}
        try:
            user = session.get('UserID')
            for giro in giroList:
                description = "GIRO Payment - " + giro.get_giroName()
                bankName = giro.get_bank().split(" ")
                if giro.get_user() == user:
                    if int(date) == 15:
                        for bank in bankList:
                            if bank.get_bankName() == bankName[0]:
                                if bank.get_accType() == bankName[1]:
                                    if str(bank.get_accountNum()) == str(giro.get_accNum()):
                                        if bank.get_accType() == "Savings":
                                            if float(bank.get_savingBal()) >= float(giro.get_giroAmt()):
                                                amount = float(giro.get_giroAmt())
                                                bankN = bankName[0]
                                                newTransaction = db_dashboard.Transaction_Hist(user, dateTrans, "4829", description, "Withdrawal", giro.get_giroAmt(), month, bankN)
                                                if bank.get_accType() == "Savings":
                                                    if newTransaction not in transactionList:
                                                        bank.withdraw_saving(amount)
                                                        bank.set_savingBal(bank.get_savingBal())
                                                        transactionList.append(newTransaction)
                                                        db2['transactions'] = transactionList
                                                        db2.close()
                                            else:
                                                bankN = bankName[0]
                                                newTransaction = db_dashboard.Transaction_Hist(user, dateTrans, "4829", description, "Withdrawal", giro.get_giroAmt(), month, bankN)
                                                for t in transactionList:
                                                    if t.get_description() != description:
                                                        if newTransaction not in transactionList:
                                                            if giroNoti.get(giro.get_giroName()) != ("Success"):
                                                                giroNoti[giro.get_giroName()] = ("GIRO payment for " + giro.get_giroName() + " have failed!")
                                                                notificationDist['GIRO'] = giroNoti
            db['banks'] = bankList
            db.close()
        except:
            pass

        try:
            for transaction in transactionList:
                for giro in giroList:
                    description = "GIRO Payment - " + giro.get_giroName()
                    if transaction.get_description() == description:
                        if transaction.get_month() == month:
                            notificationDist['GIRO'].pop(giro.get_giroName())
        except Exception as e:
            print(e)

        try:
            for transac in transactionList:
                print(transac.get_description())
        except:
            pass

        try:
            for i in bankList:
                if i.get_accType() == "Savings":
                    print(i.get_savingBal())
                else:
                    print(i.get_currentBal())
        except:
            pass
        return redirect(url_for('home'))

    if session.get('Notification') == None:
        session['Notification'] = {"Nothing": 1}
    return render_template('Login.html', title='Login', form=form)
# END OF LOGIN

# SIGNUP
class SignupForm(Form):
    username = StringField('NRIC', [validators.Regexp('^[stfgSTFG]\d{7}[a-zA-Z]$', message="Invalid NRIC"),  validators.DataRequired()])
    password = PasswordField('Password', [validators.Regexp('^[a-zA-Z]{1,}'), validators.DataRequired(), validators.Length(min=8, max=20, message="Password must be between 8 and 20 characters long."), validators.EqualTo('confirm', message='Passwords must match')])
    confirm = PasswordField('Confirm Password', [validators.Regexp('^[a-zA-Z]{1,}'), validators.DataRequired()])


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    form = SignupForm(request.form)
    userList = {}

    if request.method == 'POST' and form.validate():
        try:
            db = shelve.open('user.db', 'c')
            userList = db['Users']
        except:
            print("Fail to open database")

        user = User.User(form.username.data, form.password.data)

        userList[user.get_nric()] = user
        db['Users'] = userList

        db.close()

        return redirect(url_for('login'))

    return render_template('Signup.html', title='Signup', form=form)


# END OF SIGNUP

# LOGOUT
@app.route('/logout')
def logout():
    session.pop('UserID', None)
    session.pop('Name', None)
    session.pop('Notification', None)
    return redirect(url_for('home'))


# END OF LOGOUT


@app.route('/expenditure')
def expenditure():
    try:
        userID = session.get('UserID')
        # userID = 1
        if userID:
            datenow = datetime.today().strftime("%Y/%m/%d")
            if int(datenow[-2:]) >= 15:
                db2 = shelve.open('savings.db', 'w')
                savingsList = db2['savings']
                db3 = shelve.open('transaction.db', 'w')
                transactionList = db3['transactions']
                db = shelve.open('bank.db', 'w')
                bankList = db['banks']
                for saving in savingsList:
                    if saving.get_userID() == userID:
                        if datenow[:7] not in saving.get_lastUpdate():
                            dMeth = saving.get_depositMeth().split(",")
                            if dMeth[0] == "Auto":
                                bAcc = str(dMeth[2])
                                amount = int(dMeth[1])
                                for bank in bankList:
                                    if bank.get_userID() == userID:
                                        if bank.get_accType() == "Savings":
                                            if bank.get_accountNum() == bAcc:
                                                if bank.get_savingBal() >= amount:
                                                    bank.withdraw_saving(amount)
                                                    saving.set_savedAmount(amount)
                                                    saving.set_lastUpdate(datenow[:7])
                                                    if saving.get_savedAmount() >= saving.get_goal():
                                                        excess = saving.get_savedAmount() - saving.get_goal()
                                                        bank.deposit_saving(excess)
                                                        flash(
                                                            "Congratulation you've reached your saving goal for " + str(
                                                                saving.get_description()),
                                                            "success")
                                                    if int(datenow[5:7]) == 1:
                                                        month = db_dashboard.get_StrMonth(13)
                                                    else:
                                                        month = db_dashboard.get_StrMonth(int(datenow[5:7]))
                                                    newTranz = db_dashboard.Transaction_Hist(userID, datenow, "4829",
                                                                                             "Deposit into Saving",
                                                                                             'Withdrawal', amount,
                                                                                             month, bank.get_bankName())
                                                    transactionList.append(newTranz)
                                                else:
                                                    # notifiDB = shelve.open("user.db", "w")
                                                    # userList = notifiDB['Users']
                                                    # del notifiDB["Users"]
                                                    # for user in userList:
                                                    #     if user.get_userID() == userID:
                                                    #         user.set_notifications("Insufficient Funds to deduct for ",
                                                    #                                saving.get_description())
                                                    # notifiDB["Users"] = userList
                                                    # notifiDB.close()
                                                    flash(
                                                        "Insufficient Funds to deduct for " + saving.get_description(),
                                                        "danger")
                                                    continue
                                            else:
                                                continue
                                        else:
                                            continue
                                    else:
                                        continue
                            else:
                                continue
                        else:
                            continue
                    else:
                        continue
                del db["banks"]
                db['banks'] = bankList
                db.close()
                del db3["transactions"]
                db3['transactions'] = transactionList
                db3.close()
                del db2["savings"]
                db2['savings'] = savingsList
                db2.close()
            banks = {}
            savings = []
            MIList = []
            MOList = []
            db = shelve.open('bank.db', 'r')
            bankList = db['banks']
            db.close()
            db2 = shelve.open('savings.db', 'r')
            savingsList = db2['savings']
            db2.close()
            db3 = shelve.open('transaction.db', 'r')
            transactionList = db3['transactions']
            db3.close()
            for bank in bankList:
                if bank.get_userID() == userID:
                    if bank.get_bankName() not in banks:
                        banks[bank.get_bankName()] = [bank]
                    else:
                        banks[bank.get_bankName()].append(bank)
            for saving in savingsList:
                if saving.get_userID() == userID:
                    savings.append(saving)

            months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            monthnow = datetime.today().month
            if int(monthnow) == 1:
                monthnow = db_dashboard.get_StrMonth(13)
            else:
                monthnow = db_dashboard.get_StrMonth(int(monthnow))
            monthindex = months.index(monthnow)
            newmonths = months[monthindex:]
            del months[monthindex:]
            months = newmonths + months
            for month in months:
                totalMI = 0
                totalMO = 0
                for transaction in transactionList:
                    if transaction.get_userID() == userID:
                        if transaction.get_month() == month:
                            if transaction.get_type() == "Deposit":
                                totalMI += transaction.get_amount()
                            else:
                                totalMO += transaction.get_amount()
                        else:
                            continue
                MIList.append(totalMI)
                MOList.append(totalMO)

            # # render bar chart
            bar_chart = pygal.Bar()
            bar_chart.title = 'Deposit Vs Withdrawal Across All Banks'
            bar_chart.x_labels = months
            bar_chart.add("Deposit", MIList)  # income
            bar_chart.add("Withdrawal", MOList)  # spending
            bar = bar_chart.render_data_uri()
            date = datetime.today().strftime("%Y-%m-%d")
            return render_template("Expenditure.html", bar=bar, savings=savings, banks=banks, date=date, month=monthnow)
            # return render_template("Expenditure.html", bar=bar, savings=savings, banks=banks, date=date)
        else:
            flash("Please Login", "danger")
            return redirect(url_for('home'))
    except Exception as e:
        print(e)
    return render_template('Expenditure.html')


@app.route('/newSaving', methods=['GET', 'POST'])
def newSaving():
    try:
        userID = session.get("UserID")
        # userID = 1
        form = newSavings(request.form)
        if request.method == "POST":
            db = shelve.open('savings.db', 'w')
            savingList = db['savings']
            del db["savings"]
            # if isinstance(form.savingDeadline.data, datetime):
            #     deadline = form.savingDeadline.data.strftime("%Y-%m-%d")
            # else:
            #     deadline = datetime.strptime(str(form.savingDeadline.data), "%Y-%m-%d").strftime("%Y-%m-%d")
            if isinstance(form.savingDeadline.data, datetime):
                deadline = form.savingDeadline.data
            else:
                deadline = datetime.strptime(str(form.savingDeadline.data), "%Y-%m-%d")
                deadline = deadline.strftime("%Y-%m-%d")
            saving = Savings.Saving(userID, form.savingType.data, int(form.savingGoal.data), deadline,
                                    form.savingDescription.data)
            if form.savingSavers.data:
                saving.set_savers(form.savingSavers.data)
            if form.savingMethod.data == "Manually":
                saving.set_depositMeth(form.savingMethod.data, 0, 0)
            else:
                saving.set_depositMeth(form.savingMethod.data, form.savingAmount.data, form.bankAccounts.data)
            savingList.append(saving)
            db['savings'] = savingList
            db.close()
            flash("Savings Created", "success")
            return redirect(url_for("expenditure"))
        db = shelve.open('bank.db', 'r')
        bankList = db['banks']
        db.close()
        # populate before rendering
        form.bankAccounts.choices = []
        for bank in bankList:
            if bank.get_userID() == userID:
                if (bank.get_accountNum(), bank.get_accountNum()) not in form.bankAccounts.choices:
                    form.bankAccounts.choices.append((bank.get_accountNum(), bank.get_accountNum()))
                else:
                    continue
        datenow = datetime.today().strftime("%Y/%m/%d")
        date = datetime.today().strftime("%Y-%m-%d")
        return render_template('newSavings.html', form=form, date=date)
    except Exception as e:
        print(e)


@app.route('/viewSavings/<int:savingID>', methods=['GET', 'POST'])
def viewSavings(savingID):
    try:
        userID = session.get("UserID")
        # userID = 1
        form = addBalance(request.form)
        if request.method == "POST":
            db = shelve.open('savings.db', 'w')
            savings = db["savings"]
            db1 = shelve.open('bank.db', 'w')
            banks = db1["banks"]
            for saving in savings:
                if saving.get_savingID() == savingID and saving.get_userID() == userID:
                    for bank in banks:
                        if bank.get_accountNum() == form.bankAccounts.data:
                            if bank.get_userID() == userID and bank.get_accType() == "Savings":
                                print(form.savingAmount.data, bank.get_savingBal())
                                if bank.get_savingBal() >= int(form.savingAmount.data):
                                    datenow = datetime.today().strftime("%Y/%m/%d")
                                    if int(datenow[5:7]) == 1:
                                        month = db_dashboard.get_StrMonth(13)
                                    else:
                                        month = db_dashboard.get_StrMonth(int(datenow[5:7]))
                                    saving.set_savedAmount(int(form.savingAmount.data))
                                    bank.withdraw_saving(int(form.savingAmount.data))
                                    flash(
                                        "Successfully Deducted $" + form.savingAmount.data + " from " + form.bankAccounts.data,
                                        "success")
                                    db3 = shelve.open('transaction.db', 'w')
                                    transactionList = db3['transactions']
                                    newTranz = db_dashboard.Transaction_Hist(userID, datenow, "4829",
                                                                             "Deposit into saving",
                                                                             'Withdrawal', int(form.savingAmount.data),
                                                                             month, bank.get_bankName())
                                    transactionList.append(newTranz)
                                    del db3["transactions"]
                                    db3["transactions"] = transactionList
                                else:
                                    flash("Insufficient Balance", "danger")
                            else:
                                continue
                        else:
                            continue
                else:
                    continue
            del db["savings"]
            db["savings"] = savings
            del db1["banks"]
            db1["banks"] = banks
            db.close()
            db1.close()
            return redirect('/viewSavings/' + str(savingID))
        db = shelve.open('savings.db', 'r')
        savings = db["savings"]
        db.close()
        db = shelve.open('bank.db', 'r')
        banks = db["banks"]
        db.close()
        for saving in savings:
            if saving.get_savingID() == savingID and saving.get_userID() == userID:
                # saving.set_deadline(datetime.strptime(saving.get_deadline(), '%Y-%m-%d'))
                form.bankAccounts.choices = []
                for bank in banks:
                    if bank.get_userID() == userID:
                        if (bank.get_accountNum(), bank.get_accountNum()) not in form.bankAccounts.choices:
                            form.bankAccounts.choices.append((bank.get_accountNum(), bank.get_accountNum()))
                        else:
                            continue
                date = datetime.today().strftime("%Y-%m-%d")
                return render_template('viewSavings.html', saving=saving, date=date, form=form)
        flash("Error retrieving your saving", "danger")
        return redirect(url_for("expenditure"))
    except Exception as e:
        print(e)


@app.route("/editSavings/<int:savingID>", methods=['GET', 'POST'])
def editSavings(savingID):
    try:
        userID = session.get("UserID")
        # userID = 1
        form = newSavings(request.form)
        if request.method == "POST":
            db = shelve.open('savings.db', 'w')
            savings = db["savings"]
            for saving in savings:
                if saving.get_savingID() == savingID and saving.get_userID() == userID:
                    if form.savingGoal.data:
                        saving.set_goal(int(form.savingGoal.data))
                    if form.savingDeadline.data:
                        if isinstance(form.savingDeadline.data, datetime):
                            deadline = form.savingDeadline.data
                        else:
                            deadline = datetime.strptime(str(form.savingDeadline.data), "%Y-%m-%d")
                            deadline = deadline.strftime("%Y-%m-%d")
                        saving.set_deadline(deadline)
                    if form.savingSavers.data:
                        saving.set_savers(form.savingSavers.data)
                    if form.savingMethod.data:
                        if form.savingMethod.data == "Manually":
                            saving.set_depositMeth(form.savingMethod.data, 0, 0)
                        else:
                            saving.set_depositMeth(form.savingMethod.data, form.savingAmount.data,
                                                   form.bankAccounts.data)
                    if form.savingDescription.data:
                        saving.set_description(form.savingDescription.data)
                    del db["savings"]
                    db["savings"] = savings
                    db.close()
                    flash("Saving Updated Successfully", "success")
                    return redirect("/viewSavings/" + str(savingID))
        db = shelve.open('savings.db', 'r')
        savings = db["savings"]
        db.close()
        # populate fields before rendering
        for saving in savings:
            if saving.get_savingID() == savingID:
                if saving.get_userID() == userID:
                    form.savingType.data = saving.get_type()
                    if saving.get_type() != "Personal":
                        form.savingSavers.data = saving.get_savers()
                    form.savingGoal.data = saving.get_goal()
                    if isinstance(saving.get_deadline(), datetime):
                        deadline = saving.get_deadline()
                    else:
                        deadline = datetime.strptime(str(saving.get_deadline()), "%Y-%m-%d")
                    form.savingDeadline.data = deadline
                    savingMethod = saving.get_depositMeth()
                    savingMethod = savingMethod.split(",")
                    form.savingMethod.data = savingMethod[0]
                    db = shelve.open('bank.db', 'r')
                    bankList = db['banks']
                    db.close()
                    form.bankAccounts.choices = []
                    if form.savingMethod.data == "Auto":
                        form.savingAmount.data = int(savingMethod[1])
                        form.bankAccounts.choices.append((savingMethod[2], savingMethod[2]))
                    for bank in bankList:
                        if bank.get_userID() == userID:
                            if (bank.get_accountNum(), bank.get_accountNum()) not in form.bankAccounts.choices:
                                form.bankAccounts.choices.append((bank.get_accountNum(), bank.get_accountNum()))
                            else:
                                continue
                    form.savingDescription.data = saving.get_description()
                    date = datetime.today().strftime("%Y-%m-%d")
                    if isinstance(date, datetime):
                        date = date
                    else:
                        date = datetime.strptime(str(date), "%Y-%m-%d")
                        date = date.strftime("%Y-%m-%d")
                    return render_template('editSavings.html', form=form, saving=saving, date=date)
                    # return render_template('editSavings.html', form=form, saving=saving)
    except Exception as e:
        print(e)


@app.route('/deleteSavings/<int:savingID>')
def deleteSavings(savingID):
    try:
        userID = session.get("UserID")
        userID = 1
        datenow = datetime.today().strftime("%Y/%m/%d")
        db = shelve.open('savings.db', 'w')
        savingList = db['savings']
        db2 = shelve.open('bank.db', 'w')
        bankList = db2['banks']
        db3 = shelve.open('transaction.db', 'w')
        transactionList = db3['transactions']
        for saving in savingList:
            if saving.get_savingID() == savingID and saving.get_userID() == userID:
                dMeth = saving.get_depositMeth()
                dMeth = dMeth.split(",")
                savedAmount = saving.get_savedAmount()
                savingList.remove(saving)
                for bank in bankList:
                    if bank.get_userID() == userID:
                        if bank.get_accType() == "Savings":
                            if dMeth[0] != "Auto":
                                bank.deposit_saving(savedAmount)
                            else:
                                if bank.get_accountNum() == dMeth[2]:
                                    bank.deposit_saving(savedAmount)
                                    if int(datenow[5:7]) == 1:
                                        month = db_dashboard.get_StrMonth(13)
                                    else:
                                        month = db_dashboard.get_StrMonth(int(datenow[5:7]))
                                    newTranz = db_dashboard.Transaction_Hist(userID, datenow, "4829",
                                                                             "Saving Cash out",
                                                                             'Deposit', savedAmount,
                                                                             month, bank.get_bankName())
                                    transactionList.append(newTranz)
                                else:
                                    continue
        del db["savings"]
        db['savings'] = savingList
        db.close()
        del db2["banks"]
        db2['banks'] = bankList
        db2.close()
        del db3["transactions"]
        db3['transactions'] = transactionList
        db3.close()
        flash("Saving Cashed out", "success")
        return redirect(url_for('expenditure'))
    except Exception as e:
        print(e)

# END OF EXPENTDITURE SAVING



# EXPENDITURE DASHBOARD
@app.route('/expenditure_payment/<string:bank>/<string:month>')
def expenditure_payment(bank, month):
    userID = session.get('UserID')
    novlist = []
    declist = []
    janlist = []
    dictlist = {}
    list1 = []
    feblist = []
    marlist = []
    code = ''
    date_list = ''
    now = datetime.now().date()
    date_now = now.day
    month_now = now.month
    count = 0
    try:
        db = shelve.open('transaction.db', 'r')
        transactions = db['transactions']
        db.close()
        for transaction in transactions:
            if transaction.get_userID() == userID:
                list1.append(transaction)
        for transaction in transactions:
            if transaction.get_userID() == userID:
                if transaction.get_bank() == bank:
                    if transaction.get_type() == "Withdrawal":
                        if transaction.get_month() == "Nov":
                            novlist.append(transaction)
                        elif transaction.get_month() == "Dec":
                            declist.append(transaction)
                        elif transaction.get_month() == "Jan":
                            janlist.append(transaction)
                        elif transaction.get_month() == 'Feb':
                            feblist.append(transaction)
                        elif transaction.get_month() == 'Mar':
                            marlist.append(transaction)
                        else:
                            print("Invalid month")

        for n in novlist:
            for d in declist:
                for j in janlist:
                    if n.get_date()[8:10] == d.get_date()[8:10] == j.get_date()[8:10]:
                        if n.get_description() == d.get_description() == j.get_description():
                            if n.get_amount() == d.get_amount() == j.get_amount():
                                dictlist[j.get_date()] = [j.get_category(), j.get_description(), j.get_type(),
                                                          j.get_amount()]
        print(dictlist)
        for date, newlist in dictlist.items():
            if bank == "DBS":
                old_date = datetime.strptime(date, '%Y/%m/%d')
                new_date = (old_date + relativedelta(months=1)).date()
                new_date = str(new_date).replace("-", "/")
                dictlist[new_date] = dictlist.pop(date)
            elif bank == "UOB":
                old_date = datetime.strptime(date, '%Y/%m/%d')
                new_date = (old_date + relativedelta(months=0)).date()
                new_date = str(new_date).replace("-", "/")
                dictlist[new_date] = dictlist.pop(date)
        db = shelve.open('payment.db', 'c')
        db["payment"] = dictlist
        db.close()
        db = shelve.open('payment.db', 'r')
        similar_transactions = db["payment"]
        db.close()
        for date, list in similar_transactions.items():
            if date[6:7] <= str(month_now):
                if int(date[6:7]) == 1:
                    date_list = db_dashboard.get_StrMonth(13)
                elif int(date[6:7]) == 2:
                    date_list = db_dashboard.get_StrMonth(2)
                elif int(date[6:7]) == 3:
                    date_list = db_dashboard.get_StrMonth(3)
                else:
                    print('error')
        for date, list in similar_transactions.items():
            if date[6:7] <= str(month_now):
                db = shelve.open('bank.db', 'w')
                bankList = db['banks']

                allbanks = {}
                for banks in bankList:
                    if banks.get_userID() == userID:
                        if banks.get_bankName() not in allbanks:
                            allbanks[banks.get_bankName()] = [banks]
                        else:
                            allbanks[banks.get_bankName()].append(banks)
                for date, list in similar_transactions.items():
                    for banks in bankList:
                        if banks.get_bankName() == bank:
                            if banks.get_accType() == "Savings":
                                if float(banks.get_savingBal()) <= float(list[3]):
                                    flash("Monthly payment for " + list[1] + " have failed!", "danger")
                                else:
                                    amount = float(list[3])
                                    banks.withdraw_saving(amount)
                                    # banks.set_savingBal(banks.get_savingBal())
                                    del db['banks']
                                    db['banks'] = bankList
                db.close()
        lastupdate = []
        for date, list in similar_transactions.items():
            if date[6:7] <= str(month_now):
                if date[8:10] < str(date_now):
                    code = db_dashboard.get_categoriesmcc(list[0])
                    newTransaction = db_dashboard.Transaction_Hist(userID, date, code, list[1], list[2],
                                                                   list[3],
                                                                   date_list, bank)
                    db = shelve.open('transaction.db', 'w')
                    transactions = db['transactions']
                    for transaction in transactions:
                        if date not in lastupdate:
                            if transaction.get_userID() == userID:
                                if transaction.get_bank() == bank:
                                    if transaction.get_month() == month:
                                        # if date == transaction.get_date():
                                        #     if list[0] == transaction.get_category():
                                        #         if list[1] == transaction.get_description():
                                        #             if list[2] == transaction.get_type():
                                        #                 if list[3] == transaction.get_amount():
                                        #                     pass
                                        #                 else:
                                        lastupdate.append(date)
                                        list1.append(newTransaction)
                                        db['transactions'] = list1

                                    else:
                                        continue
                                else:
                                    continue
                            else:
                                continue
                        else:
                            break
                    db.close()
            elif date[6:7] <= str(month_now):
                if date[8:10] >= str(date_now):
                    code = db_dashboard.get_categoriesmcc(list[0])
                    newTransaction = db_dashboard.Transaction_Hist(userID, date, code, list[1], list[2], list[3],
                                                                   date_list, bank)
                    db = shelve.open('transaction.db', 'w')
                    transactions = db['transactions']
                    for transaction in transactions:
                        print(lastupdate)
                        if date not in lastupdate:
                            if transaction.get_userID() == userID:
                                if transaction.get_bank() == bank:
                                    if transaction.get_month() == month:
                                        # if date == transaction.get_date():
                                        #     if list[0] == transaction.get_category():
                                        #         if list[1] == transaction.get_description():
                                        #             if list[2] == transaction.get_type():
                                        #                 if list[3] == transaction.get_amount():
                                        #                     pass
                                        #                 else:
                                        lastupdate.append(newTransaction)
                                        list1.append(newTransaction)
                                        db['transactions'] = list1
                                        break
                    db.close()
            elif date[8:10] == str(date_now):
                if date[8:10] <= str(date_now):
                    code = db_dashboard.get_categoriesmcc(list[0])
                    newTransaction = db_dashboard.Transaction_Hist(userID, date, code, list[1], list[2],
                                                                   list[3], date_list, bank)
                    db = shelve.open('transaction.db', 'w')
                    transactions = db['transactions']
                    for transaction in transactions:
                        if date not in lastupdate:
                            if transaction.get_userID() == userID:
                                if transaction.get_bank() == bank:
                                    if transaction.get_month() == month:
                                        # if date == transaction.get_date():
                                        #     if list[0] == transaction.get_category():
                                        #         if list[1] == transaction.get_description():
                                        #             if list[2] == transaction.get_type():
                                        #                 if list[3] == transaction.get_amount():
                                        #                     pass
                                        #                 else:
                                        lastupdate.append(date)
                                        list1.append(newTransaction)
                                        db['transactions'] = list1
                                        break
                    db.close()

        for date, newlist in similar_transactions.items():
            if date[6:7] == str(month_now):
                if date[8:10] <= str(date_now):
                    old_date = datetime.strptime(date, '%Y/%m/%d')
                    new_date = (old_date + relativedelta(months=1)).date()
                    new_date = str(new_date).replace("-", "/")
                    similar_transactions[new_date] = similar_transactions.pop(date)
            elif date[6:7] < str(month_now):
                if date[8:10] >= str(date_now):
                    old_date = datetime.strptime(date, '%Y/%m/%d')
                    new_date = (old_date + relativedelta(months=1)).date()
                    new_date = str(new_date).replace("-", "/")
                    similar_transactions[new_date] = similar_transactions.pop(date)
                elif date[8:10] <= str(date_now):
                    old_date = datetime.strptime(date, '%Y/%m/%d')
                    new_date = (old_date + relativedelta(months=2)).date()
                    new_date = str(new_date).replace("-", "/")
                    similar_transactions[new_date] = similar_transactions.pop(date)
            count += 1
        session['Expenditure_D1'] = count

        return render_template("Expenditure_payments.html", month=month, bank=bank,
                               similar_transactions=similar_transactions)

    except Exception as e:
        return (print(e))


class TransactionForm(Form):
    date = DateField('Date of Transaction:', format='%Y-%m-%d')
    # myChoices = []
    # try:
    #     db = shelve.open('mcc.db', 'r', writeback=True)
    #     for codes, category in db.items():
    #         myChoices.append(category)
    # except Exception as e:
    #     print(e)
    categories_code = SelectField('Category:', choices=[("4829", "Money Orders - Wire Transfer"),
                                                        ("4900", "Electric, Gas, Sanitary and Water Utilities"),
                                                        ("5013", "Motor vehicle supplies"),
                                                        ("5045", "Computers, Computer Equipment, Software"),
                                                        ("5944", "Watch, Clock, Jewelry, and Silverware Stores"),
                                                        ("5311", "Department Stores"),
                                                        ("5411", "Supermarkets"),
                                                        ("5812", "Eating places and Restaurants"),
                                                        ("5813", "Drinking Places(Bars and Nightclubs)"),
                                                        ("5814", "Fast Food Restaurants"),
                                                        ("5941", "Sporting Goods Stores"),
                                                        ("5942", "Book Stores"),
                                                        ("6381", "Insurance Premiums"),
                                                        ("7997", "Membership Clubs"),
                                                        ("9311", "Tax Payments"),
                                                        ("8071", "Medical and Dental Laboratories"),
                                                        ("5309", "Duty Free Store")
                                                        ])
    description = TextAreaField('Description:', "")
    choice = RadioField('Type:', choices=[('Deposit', 'Deposit'), ('Withdrawal', 'Withdrawal')], default='')
    amount = IntegerField('Amount:', "")


@app.route('/expenditure_Dash/<string:bank>/<string:month>', methods=['GET', 'POST'])
def expenditure_Dash(bank, month):
    budget_list = ["None", 0, 0]
    userID = session.get('UserID')
    bar = pygal.Bar()
    a = 0
    b = 0
    c = 0
    d = 0
    week1 = {}
    week2 = {}
    week3 = {}
    week4 = {}
    list1 = []
    new_dic1 = {}
    new_dic2 = {}
    new_dic3 = {}
    new_dic4 = {}
    transactionlist = []
    transactions = {}
    jan = 0
    feb = 0
    mar = 0
    months = ['Nov', 'Dec', 'Jan', 'Feb']
    form = TransactionForm(request.form)
    withdrawal = 0
    deposit = 0
    current_month = 0
    previous_month = 0
    previous = 0
    difference = 0
    list1 = []
    count = 0
    banks = {}
    savings = []
    try:
        db = shelve.open('transaction.db', 'r')
        transactions = db["transactions"]
        db.close()
        db = shelve.open('bank.db', 'r')
        bankList = db['banks']
        banks = []
        bankobj = []
        for bank1 in bankList:
            # if bank1.get_bankName() == bank:
            if bank1.get_userID() == userID:
                if bank1.get_accType() == "Savings":
                    bankobj.append(bank1)
                    if bank1.get_bankName() not in banks:
                        banks.append(bank1.get_bankName())
                    else:
                        continue
                else:
                    continue
            else:
                continue
        db.close()
        for transac_hist in transactions:
            if transac_hist.get_userID() == userID:
                for obj in bankobj:
                    if transac_hist.get_bank() == obj.get_bankName():
                        if obj.get_accType() == "Savings":
                            transac_hist.set_balance(obj.get_savingBal())
                            transactionlist.append(transac_hist)
                        else:
                            continue
                    else:
                        continue
            else:
                continue

        for i in transactions:
            if i.get_userID() == userID:
                if i.get_bank() == bank:
                    if i.get_month() == month:
                        previous = i.get_IntMonth() - 1
                        if i.get_type() == "Withdrawal":
                            current_month += i.get_amount()
                        else:
                            continue
                    else:
                        continue
                else:
                    continue
            else:
                continue

        for x in transactions:
            if x.get_userID() == userID:
                if x.get_bank() == bank:
                    if db_dashboard.get_StrMonth(previous) == x.get_month():
                        # print(x.get_month())
                        if x.get_type() == "Withdrawal":
                            previous_month += x.get_amount()
                        else:
                            continue
                    else:
                        continue
                else:
                    continue
            else:
                continue

        if previous_month == 0:
            difference = current_month
        else:
            difference = (current_month - previous_month) / previous_month * 100
            if difference < 0:
                difference *= -1

        for transaction in transactions:
            if transaction.get_userID() == userID:
                if transaction.get_bank() == bank:
                    if transaction.get_month() == month:
                        if transaction.get_type() == "Withdrawal":
                            withdrawal += transaction.get_amount()
                        elif transaction.get_type() == "Deposit":
                            deposit += transaction.get_amount()
                        bar_chart = pygal.Bar()
                        bar_chart.title = 'Deposits Vs Spending'
                        bar_chart.x_labels = [month]
                        bar_chart.add('Deposits', deposit)
                        bar_chart.add('Withdrawals', withdrawal)
                        bar = bar_chart.render_data_uri()
                    else:
                        continue
                else:
                    continue
            else:
                continue

        # Timeline
        try:
            for transaction in transactions:
                if transaction.get_userID() == userID:
                    if transaction.get_bank() == bank:
                        if transaction.get_month() == month:
                            if transaction.get_type() == "Withdrawal":
                                date = int(transaction.get_date()[8:10])
                                if date <= 7:
                                    week1[transaction.get_date()] = []
                                elif 7 < date <= 14:
                                    week2[transaction.get_date()] = []
                                elif 14 < date <= 21:
                                    week3[transaction.get_date()] = []
                                else:
                                    week4[transaction.get_date()] = []
                            else:
                                continue
                        else:
                            continue
                    else:
                        continue
                else:
                    continue

            for transaction in transactions:
                if transaction.get_userID() == userID:
                    if transaction.get_bank() == bank:
                        if transaction.get_month() == month:
                            if transaction.get_type() == "Withdrawal":
                                # Week1
                                for date, amount in week1.items():
                                    if transaction.get_date() == date:
                                        week1[date].append(transaction.get_amount())
                                        a = sum(sum(x) for x in week1.values())

                                for date, amount in week1.items():
                                    new_dic1[date] = sum(amount)
                                # Week2
                                for date, amount in week2.items():
                                    if transaction.get_date() == date:
                                        week2[date].append(transaction.get_amount())
                                    b = sum(sum(x) for x in week2.values())

                                for date, amount in week2.items():
                                    new_dic2[date] = sum(amount)

                                # Week3
                                for date, amount in week3.items():
                                    if transaction.get_date() == date:
                                        week3[date].append(transaction.get_amount())
                                    c = sum(sum(x) for x in week3.values())

                                for date, amount in week3.items():
                                    new_dic3[date] = sum(amount)

                                # Week4
                                for date, amount in week4.items():
                                    if transaction.get_date() == date:
                                        week4[date].append(transaction.get_amount())
                                    d = sum(sum(x) for x in week4.values())

                                for date, amount in week4.items():
                                    new_dic4[date] = sum(amount)



        except Exception as e:
            print(str(e))
        # Add transaction form request
        if request.method == "POST":
            db = shelve.open('transaction.db', 'w')
            # del db['transactions']
            form.date.data = datetime.strptime(str(form.date.data), "%Y-%m-%d")
            form.date.data = str(form.date.data).replace("-", "/")
            form.amount.data = int(form.amount.data)
            newTransaction = db_dashboard.Transaction_Hist(userID, form.date.data, form.categories_code.data,
                                                           form.description.data,
                                                           form.choice.data, form.amount.data, month, bank)
            for transaction in transactions:
                list1.append(transaction)

            # # Insurance
            # try:
            #     db = shelve.open('insurance.db', 'r')
            #     insuranceList = db['Accounts']
            #     db.close()
            # except Exception as e:
            #     print(e)
            #
            # x = datetime.now()
            # dateList = str(x).split(" ")
            # dateList.pop(1)
            # for i in dateList:
            #     dateList = i.split("-")
            # dateTrans = dateList[0] + "/" + dateList[1] + "/" + dateList[2]
            # date = dateList[-1]
            #
            # for insurance in insuranceList:
            #     if insuranceList[insurance].get_user() == session.get('UserID'):
            #         insuranceDate = insuranceList[insurance].get_startDate().split("-")
            #         insuranceDate = insuranceDate[-1]
            #         if insuranceDate == date:
            #             if int(insuranceList[insurance].get_monthlyPayment()) == form.amount.data:
            #                 insuranceSession = session.get('Notification').get('Insurance')
            #                 for insur in insuranceSession:
            #                     if insuranceSession[insur] == 0:
            #                         insuranceSession.pop(insur)

            while True:
                for transaction in transactions:
                    if transaction.get_userID == userID:
                        if transaction.get_bank() == bank:
                            if transaction.get_month() == month:
                                transactionlist.append(newTransaction)
                                db['transactions'] = transactionlist
                                transactionlist = db['transactions']
                                db.close()
                                return redirect(url_for("expenditure"))

        if session.get("Expenditure_D") == None:
            budget_list = budget_list
        else:
            budget_list = session.get("Expenditure_D")

        if session.get("Expenditure_D1") == None:
            count = count
        else:
            count = session.get("Expenditure_D1")
    except Exception as e:
        print(str(e))

    return render_template("Expenditure_dashboard.html", banks=banks, count=count,
                           transactionlist=reversed(transactionlist), bank=bank, current_month=current_month,
                           previous_month=previous_month, difference=difference, jan=jan, feb=feb, mar=mar,
                           withdrawal=withdrawal, form=form, bar=bar, list1=list1, week1=week1, week2=week2,
                           week3=week3, week4=week4, months=months, a=a, b=b, c=c, d=d, new_dic1=new_dic1,
                           new_dic2=new_dic2, new_dic3=new_dic3, new_dic4=new_dic4, month=month,
                           budget_list=budget_list)

@app.route('/week_<int:week>/<string:bank>/<string:month>')
def week(week, bank, month):
    userID = session.get('UserID')
    # Timeline
    weekitems = []
    try:
        # db = shelve.open('transaction.db', 'r')
        # transactions = db["transactions"]
        # db.close()
        # db = shelve.open('bank.db', 'r')
        # bankList = db['banks']
        # banks = []
        # bankobj = []
        # for bank1 in bankList:
        #     # if bank1.get_bankName() == bank:
        #     if bank1.get_userID() == userID:
        #         if bank1.get_accType() == "Savings":
        #             bankobj.append(bank1)
        #             if bank1.get_bankName() not in banks:
        #                 banks.append(bank1.get_bankName())
        #             else:
        #                 continue
        #         else:
        #             continue
        #     else:
        #         continue
        # db.close()
        # for transac_hist in transactions:
        #     if transac_hist.get_userID() == userID:
        #         for obj in bankobj:
        #             if transac_hist.get_bank() == obj.get_bankName():
        #                 if obj.get_accType() == "Savings":
        #                     transac_hist.set_balance(obj.get_savingBal())
        #                     transactionlist.append(transac_hist)
        #                 else:
        #                     continue
        #             else:
        #                 continue
        #     else:
        #         continue
        db = shelve.open('transaction.db', 'r', writeback=True)
        weeks = db['transactions']
        for a in weeks:
            if a.get_userID() == userID:
                if a.get_bank() == bank:
                    if a.get_type() == "Withdrawal":
                        if week == 1:
                            if int(a.get_numDate()) <= 7:
                                weekitems.append(a)
                        elif week == 2:
                            if 7 < int(a.get_numDate()) <= 14:
                                weekitems.append(a)
                        elif week == 3:
                            if 14 < int(a.get_numDate()) <= 21:
                                weekitems.append(a)
                        elif week == 4:
                            if 21 < int(a.get_numDate()) <= 31:
                                weekitems.append(a)
                        else:
                            weekitems.append("invalid")

        db.close()
    except Exception as e:
        print(str(e))

    return render_template("weeks.html", bank=bank, month=month, weekitems=weekitems, week=week)


@app.route('/expenditure_tspending/<string:bank>/<string:month>')
def expenditure_tspending(bank, month):
    # try:
    #     db = shelve.open('bank.db', 'r')
    #     bankList = db['banks']
    #     db.close()
    #     for banks in bankList:
    #         if userID == banks.get_userID():
    #             if banks.get_bankName() == bank:
    #
    #
    # except Exception as e:
    #     print(e)
    try:
        # Pie-chart
        nochart = {}
        nochart1 = {}
        db = shelve.open('transaction.db', 'r', writeback=True)
        transactions = db['transactions']
        db.close()
        pie_chart = pygal.Pie()
        pie_chart.title = 'Spendings for %s' % month
        percent_formatter = lambda x: '.2f%'.format(x)
        dollar_formatter = lambda x: '${:.10g}'.format(x)
        pie_chart.value_formatter = percent_formatter
        for transaction in transactions:
            if transaction.get_bank() == bank:
                if transaction.get_month() == month:
                    if transaction.get_type() == "Withdrawal":
                        if transaction.get_category() not in nochart:
                            nochart[transaction.get_category()] = [transaction.get_amount()]
                        elif transaction.get_category() in nochart:
                            nochart[transaction.get_category()].append(transaction.get_amount())

        for category, amount in nochart.items():
            nochart1[category] = sum(amount)
        for category, amount in nochart1.items():
            pie_chart.add(category, amount, formatter=dollar_formatter)
        pie = pie_chart.render_data_uri()
        return render_template("Expenditure_totalspending.html", nochart1=nochart1, bank=bank, pie=pie,
                               transactions=transactions, month=month)

    except Exception as e:
        print(str(e))


class BudgetForm(Form):
    budget = IntegerField("")
    categories_code = SelectField('Category:', choices=[("4900", "Electric, Gas, Sanitary and Water Utilities"),
                                                        ("5013", "Motor vehicle supplies"),
                                                        ("5045", "Computers, Computer Equipment, Software"),
                                                        ("5944", "Watch, Clock, Jewelry, and Silverware Stores"),
                                                        ("5311", "Department Stores"),
                                                        ("5411", "Supermarkets"),
                                                        ("5812", "Eating places and Restaurants"),
                                                        ("5813", "Drinking Places(Bars and Nightclubs)"),
                                                        ("5814", "Fast Food Restaurants"),
                                                        ("5941", "Sporting Goods Stores"),
                                                        ("5942", "Book Stores"),
                                                        ("6381", "Insurance Premiums"),
                                                        ("7997", "Membership Clubs"),
                                                        ("9311", "Tax Payments"),
                                                        ("8071", "Medical and Dental Laboratories"),
                                                        ("5309", "Duty Free Store")
                                                        ])


@app.route('/expenditure_formbudget/<string:bank>/<string:month>', methods=['GET', 'POST'])
def expenditure_formbudget(bank, month):
    userID = session.get("UserID")
    gau = pygal.SolidGauge()
    # try:
    #     db = shelve.open('bank.db', 'r')
    #     bankList = db['banks']
    #     db.close()
    #     for banks in bankList:
    #         if userID == banks.get_userID():
    #             if banks.get_bankName() == bank:
    #
    #
    # except Exception as e:
    #     print(e)
    a = 0
    d = 0
    try:
        list = []
        dbList = []
        budgetset = {}
        budgetactual = {}
        actualamount = {}
        notidict = {}
        notilist = []
        form = BudgetForm(request.form)
        db = shelve.open("budget.db", 'w')
        dict = db['Budget']

        # if request.method == "POST":
        budget = db_dashboard.Budget(userID, form.budget.data, form.categories_code.data, month)
        dict[budget.get_category()] = budget.get_amount()
        db["Budget"] = dict
        displaydict = db["Budget"]

        mcc = shelve.open("mcc.db", 'r')
        db1 = shelve.open('transaction.db', 'r')
        transactions = db1['transactions']
        for transaction in transactions:
            if transaction.get_bank() == bank:
                if transaction.get_month() == month:
                    if transaction.get_type() == "Withdrawal":
                        if transaction.get_category() not in budgetactual:
                            budgetactual[transaction.get_category()] = [transaction.get_amount()]
                        elif transaction.get_category() in budgetactual:
                            budgetactual[transaction.get_category()].append(transaction.get_amount())
                        else:
                            print("error")

                    for category, amount in budgetactual.items():
                        actualamount[category] = sum(amount)

            db.close()
            mcc.close()
            db.close()
            gauge = pygal.SolidGauge(inner_radius=0.70)
            gauge.title = 'Budget for %s' % month
            percent_formatter = lambda x: '{:.10g}%'.format(x)
            dollar_formatter = lambda x: '${:.10g}'.format(x)
            gauge.value_formatter = percent_formatter
            for category, amount in displaydict.items():
                for cat, drawings in actualamount.items():
                    if category == cat:
                        a = amount
                        d = drawings
                        gauge.add(category, [{'value': drawings, 'max_value': int(amount)}],
                                  formatter=dollar_formatter)
            gau = gauge.render_data_uri()

            session['Expenditure_D'] = [category, float(d), float(a)]
        notificationDist = {}
        for category, amount in displaydict.items():
            for cat, drawings in actualamount.items():
                if category == cat:
                    percentage = drawings / amount * 100
                    notidict[category] = percentage
        for category, percentage in notidict.items():
            if 80 <= percentage < 100:
                noti = "You have spend more than 80% of your budget for " + category + "!!"
                notilist.append(noti)
            elif percentage > 100:
                noti = "You have spend all of your budget for " + category + "!!"
                notilist.append(noti)
        notificationDist = session.get('Notification')
        notificationDist['Budget'] = notilist
        session['Notification'] = notificationDist
        return render_template("Expenditure_formbudget.html", bank=bank, gau=gau, form=form, list=list, month=month)

    except Exception as e:
        print(str(e))


# @app.route('/expenditure_budgetleft')
# def expenditure_budgetleft():
#     return render_template('Expenditure_budgetleft.html')

# END OF EXPENDITURE DASHBOARD

# INSURANCE ACCOUNT
class insurForm(Form):
    insuranceCompany = SelectField('Insurance Company', [validators.DataRequired()],
                                   choices=[('', 'Select'), ('AIA', 'AIA'), ('Prudential', 'Prudential'),
                                            ('FWD', 'FWD'), ('NTUC Income', 'NTUC Income')], default='')
    policyName = StringField('Policy Name',
                             [validators.Regexp('^[stfgSTFG]\d{7}[a-zA-Z]$', message="Invalid Policy Name"),
                              validators.DataRequired()])
    policyNum = StringField('Policy Number',
                            [validators.Regexp('^[stfgSTFG]\d{7}[a-zA-Z]$', message="Invalid Policy Number"),
                             validators.DataRequired()])
    policyType = SelectField('Policy Type', [validators.DataRequired()],
                             choices=[('', 'Select'), ('Accident', 'Accident Insurance'), ('Car', 'Car Insurance'),
                                      ('Health', 'Health Insurance'), ('House', 'House Insurance'),
                                      ('Life', 'Life Insurance'), ('Travel', 'Travel Insurance')], default="")
    peopleInsured = SelectField('Party Insured', [validators.DataRequired()],
                                choices=[('', 'Select'), ('Car', 'Car'), ('Family', 'Family'), ('House', 'House'),
                                         ('Myself', 'Myself'), ('Spouses', 'Spouses'), ('Parents', 'Parents')],
                                default='')
    amountInsured = IntegerField('Amount Insured', [validators.Regexp('^(?!0(.0+)?$)(\d+|\d.\d+)$')], default=0)
    paymentPeriod = IntegerField('Payment Period by Years',
                                 [validators.Regexp('^(?!0(.0+)?$)(\d+|\d.\d+)$'), validators.DataRequired()])
    monthlyPayment = IntegerField('Monthly Payment Amount',
                                  [validators.Regexp('/^[0-9]*$/'), validators.DataRequired()])
    excess = IntegerField('Excess Amount')
    startDate = DateField('Start of payment Period', format='%Y-%m-%d')

@app.route('/insuranceForm', methods=['GET', 'POST'])
def insuranceForm():
    insurFilter = session.get('insurFilter')
    form = insurForm(request.form, policyType=insurFilter)
    accountList = {}
    list = []
    accForm = accountForm(request.form)
    try:
        db = shelve.open('account.db', 'r')
        accountList = db['Accounts']
        db.close()
    except Exception as e:
        print(e)
    for account in accountList:
        list.append(accountList[account])
    if request.method == 'POST':
        try:
            startDate = form.startDate.data.strftime("%Y-%m-%d")
            user = int(session['UserID'])
            # amountInsured = form.amountInsured.data
            # assert(amountInsured > 0), "Must be more than 0"
            i = insuranceUser.insurancePolicies(form.insuranceCompany.data, form.policyName.data, form.policyNum.data,
                                                form.policyType.data, form.peopleInsured.data, form.amountInsured.data,
                                                form.paymentPeriod.data, form.monthlyPayment.data, form.excess.data,
                                                startDate, user)
            db = shelve.open('insurance.db', 'w')
            insuranceList = db['Accounts']
            insuranceList[i.get_policyNumber()] = i
            db['Accounts'] = insuranceList
            db.close()
            return redirect(url_for("insuranceAcc"))
        except Exception as e:
            print(e)
    return render_template('insuranceForm.html', form=form, accountList=list, accountForm=accForm)
@app.route('/insuranceAccDetails')
def insuranceAccDetails():
    accountList = {}
    list = []
    accForm = accountForm(request.form)
    try:
        db = shelve.open('account.db', 'r')
        accountList = db['Accounts']
        db.close()
    except Exception as e:
        print(e)
    except:
        print("Fail to open database")

    for account in accountList:
        list.append(accountList[account])
        try:
            db = shelve.open('insurance.db', 'w')
            insuranceList = db['Accounts']
            monthlyDict = {}
            insuredDict = {}
            # Charts
            pie_chart = pygal.Pie(style=DefaultStyle)
            pie_chart.title = 'Breakdown of Monthly Payable (in $)'
            bar_chart = pygal.Bar(style=DefaultStyle)
            bar_chart.title = 'Breakdown of Amount Insured (in $)'
            for i in insuranceList:
                if insuranceList[i].get_user() == session.get("UserID"):
                    if insuranceList[i].get_policyType() in monthlyDict:
                        monthlyDict[insuranceList[i].get_policyType()] += int(insuranceList[i].get_monthlyPayment())
                    else:
                        monthlyDict[insuranceList[i].get_policyType()] = int(insuranceList[i].get_monthlyPayment())

                    if insuranceList[i].get_policyType() in insuredDict:
                        insuredDict[insuranceList[i].get_policyType()] += int(insuranceList[i].get_amountInsured())
                    else:
                        insuredDict[insuranceList[i].get_policyType()] = int(insuranceList[i].get_amountInsured())
            print(monthlyDict)
            print(insuredDict)
            for insurance in monthlyDict:
                if insurance != "Travel":
                    pie_chart.add(insurance, monthlyDict[insurance])
            for a in insuredDict:
                if a != "Car":
                    bar_chart.add(a, insuredDict[a])
            pie = pie_chart.render_data_uri()
            bar = bar_chart.render_data_uri()
        except Exception as e:
            return (str(e))

        try:
            db = shelve.open('insurance.db', 'r')
            insuranceList = db['Accounts']
            db.close()
        except:
            print("Fail to open database")

        try:
            db = shelve.open('user.db', 'r')
            userList = db['Users']
        except:
            print("Fail to open database")
        db.close()
        return render_template('insuranceAccDetails.html', pie=pie, bar=bar, accountList=list, accountForm=accForm)
    else:
        flash("Do not have any insurance", "warning")
        return redirect(url_for("insuranceAcc"))
# END OF INSURANCE ACCOUNT


# END OF ACCOUNT
class insuranceFilter(Form):
    insurance_type = SelectField('', choices=[('', 'Select'), ('Accident', 'Accident'), ('Car', 'Car'), ('Health', 'Health'), ('House', 'House'), ('Life', 'Life'), ('Travel', 'Travel')], default='')
    insurance_company = SelectField('', choices=[('', 'Select'), ('AIA', 'AIA'), ('FWD', 'FWD'), ('NTUC Income', 'NTUC Income'), ('Prudential', 'Prudential')], default='')

@app.route('/insuranceAcc', methods=['GET', 'POST'])
def insuranceAcc():
    accountList = {}
    list = []
    accForm = accountForm(request.form)
    try:
        db = shelve.open('account.db', 'r')
        accountList = db['Accounts']
        db.close()
    except Exception as e:
        print(e)
    except:
        print("Fail to open database")
    for account in accountList:
        list.append(accountList[account])

    form = insuranceFilter(request.form)
    insurances = []
    insuranceList = {}
    try:
        db = shelve.open('insurance.db', 'r')
        insuranceList = db['Accounts']
        db.close()
    except Exception as e:
        print(e)
    except:
        print("Fail to open database")
    searchType = form.insurance_type.data
    if request.method == "POST":
        session['insurFilter'] = form.insurance_type.data
    for insurance in insuranceList:
        if session.get('UserID') == insuranceList[insurance].get_user():
            if insuranceList[insurance].get_policyType() == searchType:
                insurances.append(insuranceList[insurance])
            elif searchType == "":
                insurances.append(insuranceList[insurance])
    db.close()
    return render_template('insuranceAcc.html', form=form, list=insurances, accountList=list, accountForm=accForm,
                           searchType=searchType)
# END OF ACCOUNT

# INSURANCE
class insuranceSearch(Form):
    insurance_type = SelectField('', choices=[('', 'Select'), ('Accident', 'Accident'), ('Car', 'Car'), ('Health', 'Health'), ('House', 'House'), ('Life', 'Life'), ('Travel', 'Travel')], default='')
    insurance_company = SelectField('', choices=[('', 'Select'), ('AIA', 'AIA'), ('FWD', 'FWD'), ('NTUC Income', 'NTUC Income'), ('Prudential', 'Prudential')], default='')


@app.route('/insuranceMain', methods=['GET', 'POST'])
def insurance():
    form = insuranceSearch(request.form)
    accForm = accountForm(request.form)
    try:
        db = shelve.open('insurance.db', 'r')
        ownedList = db['Accounts']
        db.close()
    except Exception as e:
        print(e)
    except:
        print("Fail to open database")

    products = []
    productList = {}

    try:
        db = shelve.open('insurance.db', 'r')
        productList = db['Products']
        db.close()
    except Exception as e:
        print(e)
    except:
        print("Fail to open database")


    productTypes = ["Accident", "Car", "Health", "House", "Life", "Travel"]
    suggestList = []
    if session.get('UserID') != None:
        for owned in ownedList:
            for p in productTypes:
                if ownedList[owned].get_user() == session.get('UserID'):
                    if p == ownedList[owned].get_policyType():
                        productTypes.remove(p)
        suggestList = []

        if len(productTypes) > 3:
            productTypes.clear()

        promotionList = {}
        for p in productList:
            if p.get_promotionLink() != "":
                if len(productTypes) == 0:
                    if p.get_promotionLink() != "":
                        productList.remove(p)
                        promotionList[p.get_name()] = p

        for pr in productList:
            for pro in productTypes:
                if pr.get_type() == pro:
                    productList.remove(pr)
                    suggestList.append(pr)

        suggestList = suggestList[0:]

        for promo in promotionList:
            if len(productTypes) == 0:
                productTypes = []
                suggestList.append(promotionList[promo])

        for owned in ownedList:
            if ownedList[owned].get_user() == (session.get('UserID')):
                for product in productList:
                    if ownedList[owned].get_policyName() == product.get_name():
                        productList.remove(product)


    searchType = form.insurance_type.data
    searchCompany = form.insurance_company.data
    for product in productList:
        if product.get_type() == searchType and product.get_company() == searchCompany:
            products.append(product)
        elif product.get_type() == searchType and searchCompany == "":
            products.append(product)
        elif product.get_company() == searchCompany and searchType == "":
            products.append(product)
        elif searchCompany == "" and searchType == "":
            products.append(product)

    accountList = {}
    list = []

    try:
        db = shelve.open('account.db', 'r')
        accountList = db['Accounts']
        db.close()
    except Exception as e:
        print(e)
    for account in accountList:
        list.append(accountList[account])

    return render_template('insuranceMain.html', form=form, list=products, productTypes=productTypes, suggestList=suggestList, accountList=list, accountForm=accForm)# END OF INSURANCE


# END OF INSURANCE ACCOUNT

# # INSURANCE
# class insuranceSearch(Form):
#     insurance_type = SelectField('', choices=[('', 'Select'), ('Accident', 'Accident'), ('Car', 'Car'), ('Health', 'Health'),
#     ('House', 'House'), ('Life', 'Life'), ('Travel', 'Travel')], default='')
#     insurance_company = SelectField('', choices=[('', 'Select'), ('AIA', 'AIA'), ('FWD', 'FWD'), ('NTUC', 'NTUC Income'),
#     ('Prudential', 'Prudential')], default='')
#
# @app.route('/insuranceMain', methods=['GET', 'POST'])
# def insurance():
#     form = insuranceSearch(request.form)
#     products = []
#     productList = {}
#     try:
#         db = shelve.open('insurance.db', 'r')
#         productList = db['Products']
#         db.close()
#     except Exception as e:
#         print(e)
#     except:
#         print("Fail to open database")
#
#     searchType = form.insurance_type.data
#     searchCompany = form.insurance_company.data
#     for product in productList:
#         if product.get_type() == searchType and product.get_company() == searchCompany:
#             products.append(product)
#         elif product.get_type() == searchType and searchCompany == "":
#             products.append(product)
#         elif product.get_company() == searchCompany and searchType == "":
#             products.append(product)
#         elif searchCompany == "" and searchType == "":
#             products.append(product)
#
#     accountList = {}
#     list = []
#
#     try:
#         db = shelve.open('account.db', 'r')
#         accountList = db['Accounts']
#         db.close()
#     except:
#         print("Fail to open database")
#
#     for account in accountList:
#         list.append(accountList[account])
#
#     return render_template('insuranceMain.html', form=form, list=products, accountList=list)
# # END OF INSURANCE

# END OF INSURANCE


# INVESTMENT
@app.route('/recommendations')
def recommendations():
    stocks = ""
    # userID = 1
    userID = session.get('UserID')
    try:
        name = session.get('Name')
        db = shelve.open('Investment.db', 'r')
        recommendations = db['UserReco']
        riskList = db['UserRisk']
        registered = 0
        for i in riskList:
            if i.get_userID() != userID:
                registered = 0
            else:
                registered = 1
                break
        if registered == 0:
            return redirect('/form/' + str(userID))
        for i in recommendations:
            # if i.get_userID == session['UserID']:
            if i.get_userID() == userID:
                stocks = i.get_recommendations()
        db.close()
    except Exception as e:
        print(e)
    except:
        print("Stock not found!")
    return render_template('Investment_RecommendedStocks.html', name=name, stocks=stocks, userID=userID)

@app.route('/portfolio')
def portfolio():
    counter = 1
    stocks = ""
    # userID = 1
    userID = session.get('UserID')
    try:
        name = session.get('Name')
        db = shelve.open('Investment.db', 'r')
        portfolio = db['UserPort']
        for i in portfolio:
            if i.get_userID() == userID:
                stocks = i.get_portfolio()
                # print("portfolio: ", portfolio.index(i))
                # print(i.get_userID())
        db.close()
    except Exception as e:
        print(e)
    except Exception as e:
        print(e)

    return render_template('Investment_StockPortfolio.html', name=name, userID=userID, stocks=stocks, counter=counter)


class InvestmentSearch(Form):
    Symbol = StringField("", [validators.DataRequired()])

@app.route('/market', methods=['GET', 'POST'])
def market():
    form = InvestmentSearch(request.form)
    try:
        db = shelve.open('Investment.db', 'r')
        market = db['Market']
        stocks = market.get_market()
        db.close()
    except Exception as e:
        print(e)
    except:
        print("Stock not found!")
    if request.method == 'POST':
        try:
            db = shelve.open('Investment.db', 'w')
            market = db['Market']
            del db['Market']
            for i in market.get_market():
                if i.get_symbol() == form.Symbol.data.upper():
                    market.remove_market(i)
            market.add_market(form.Symbol.data.upper())
            db['Market'] = market
            db.close()
        except Exception as e:
            print(e)
        except:
            print("Stock not found!")
    db = shelve.open('Investment.db', 'r')
    market = db['Market']
    stocks = market.get_market()
    db.close()

    return render_template('Investment_StockMarket.html', stocks=reversed(stocks), form=form)


class InvestmentForm(Form):
    Purpose = RadioField(choices=[('goal', "Invest for life goals, allows you to acheive your immediate goals quickly through investments!"), ('general', 'General investing, allows you to grow your savings and achieve your potential goals in the future!')])
    Risk = RadioField(choices=[('protective', 'Protective'), ('stable', 'Stable'), ('balanced', 'Balanced'), ('growth', 'Growth'), ('aggressive', 'Aggressive')])
    Assessment1 = RadioField(choices=[('Yes', 'Yes'), ('No', 'No')])
    Assessment2 = RadioField(choices=[('Yes', 'Yes'), ('No', 'No')])
    Assessment3 = RadioField(choices=[('Yes', 'Yes'), ('No', 'No')])

# Please don't touch
A1 = DBI.Stocks(Stock("TSLA"))
A2 = DBI.Stocks(Stock("MSFT"))
A3 = DBI.Stocks(Stock("AAPL"))
A4 = DBI.Stocks(Stock("FB"))
A5 = DBI.Stocks(Stock("MU"))
B1 = DBI.Stocks(Stock("ISRG"))
B2 = DBI.Stocks(Stock("DVN"))
B3 = DBI.Stocks(Stock("DXC"))
B4 = DBI.Stocks(Stock("APTV"))
B5 = DBI.Stocks(Stock("HUBS"))
C1 = DBI.Stocks(Stock("GV"))
C2 = DBI.Stocks(Stock("KRNT"))
C3 = DBI.Stocks(Stock("DISCA"))
C4 = DBI.Stocks(Stock("SCHW"))
C5 = DBI.Stocks(Stock("PDCE"))
AList = [A1, A2, A3, A4, A5]
BList = [B1, B2, B3, B4, B5]
CList = [C1, C2, C3, C4, C5]
# Important lists

@app.route('/form/<int:userID>', methods=['GET', 'POST'])
def form(userID):
    try:
        userID = int(userID)
    except Exception as e:
        print(e)
    userID = session.get('UserID')
    form = InvestmentForm(request.form)
    if request.method == 'POST' and form.validate():
        db = shelve.open('Investment.db', 'w')
        try:
            riskList = db['UserRisk']
            recommendations = db['UserReco']
            del db['UserRisk']
            del db['UserReco']
        except Exception as e:
            print(e)

        userrisk = DBI.RiskScore(userID, form.Purpose.data, form.Risk.data, form.Assessment1.data, form.Assessment2.data, form.Assessment3.data)
        userrisk.calculatescore()
        for i in riskList:
            if i.get_userID() == userID:
                riskList.remove(i)
        riskList.append(userrisk)
        db['UserRisk'] = riskList
        test = DBI.Recommendations(userID)
        if 2 <= userrisk.get_score() <= 5:
            test.set_recommendations(CList)
            pass
        elif 6 <= userrisk.get_score() <= 9:
            test.set_recommendations(BList)
            pass
        else:
            test.set_recommendations(AList)
            pass
        recommendations.append(test)
        db['UserReco'] = recommendations
        db.close()
        return redirect('/recommendations')

    return render_template('Investment_Form.html', form=form)


class InvestmentOrder(Form):
    LotSize = IntegerField('No. of Lots', [validators.DataRequired(), validators.NumberRange(min=1, max=30, message="Not a valid amount")])
    CPrice = FloatField('Price', [validators.DataRequired(), validators.NumberRange(min=1, message="Not a valid amount")])
    Symbol = StringField('Symbol', [validators.DataRequired()])
    Confirm = SubmitField('Confirm')


@app.route('/buyorder/<string:id>/<string:sym>/<float:price>', methods=['GET', 'POST'])
def buyorder(id, sym, price):
    # userID = 1
    userID = session.get('UserID')
    buyorder = InvestmentOrder(request.form)
    # dbu = shelve.open('user.db', 'r')
    # users = dbb['Users']
    # for i in users:
    #     if i.get_accountID() == userID:
    #         account = i.get_preferredType()
    if request.method == 'POST':
        if buyorder.Symbol.data == sym:
            investorder = DBI.Order(DBI.Stocks(Stock(sym)), buyorder.LotSize.data, buyorder.CPrice.data, userID)
            investorder.set_buyorder()
            investorder.set_status(2)
            investorder.set_number()
            # investorder.validatestatus(account)
            # if investorder.get_status == "Pending":
            if 1 == 1:
                db = shelve.open('Investment.db', 'w')
                # db = shelve.open('Investment.db', 'w')
                # dbb = shelve.open('bank.db', 'w')

                try:
                    history = db['UserHist']
                    buy = db['buy']
                    del db['UserHist']
                    del db['buy']
                    buy.append(investorder)
                    db['buy'] = buy
                    for i in history:
                        if i.get_userID() == userID:
                            i.add_history(investorder)
                        # else:
                        #     instance2 = DBI.TransactionHistory()
                        #     i.add_history(investorder)
                        #     history.append(instance2)
                    db['UserHist'] = history
                    db.close()
                    return redirect('/investhist')
                except Exception as e:
                    print(e)
                except:
                    print("Fail to open database")
                finally:
                    DBI.p2p()

            # else:
            #     flash("You have insufficient funds in your bank account!", "danger")
            #     return redirect('/buyorder/' + id + '/' + sym + '/' + str(price))
    return render_template('Investment_OrderBuy.html', id=id, sym=sym, price=price, form=buyorder)


@app.route('/sellorder/<string:id>/<string:sym>/<float:price>/<int:counter>', methods=['GET', 'POST'])
def sellorder(id, sym, price, counter):
    # userID = 2
    userID = session.get('UserID')
    sellorder = InvestmentOrder(request.form)
    db = shelve.open('Investment.db', 'r')
    # dbu = shelve.open('user.db', 'r')
    # users = dbb['Users']
    # for i in users:
    #     if i.get_accountID() == userID:
    #         account = i.get_preferredType()
    # db = shelve.open('Investment.db', 'r')
    portfolio = db['UserPort']
    for i in portfolio:
        if i.get_userID() == userID:
            userport = i
            for j in userport.get_portfolio():
                if j.get_symbol() == sym:
                    sstock = j
                    db.close()
                    break
    if request.method == 'POST':
        if sellorder.Symbol.data == sym:
            investorder = DBI.Order(DBI.Stocks(Stock(sym)), sellorder.LotSize.data, sellorder.CPrice.data, userID)
            investorder.set_sellorder()
            investorder.set_status(2)
            investorder.set_number()
            if sellorder.CPrice.data < sstock.get_boughtPrice():
                if int(counter) == 1:
                    flash("You are selling at a loss!", "danger")
                    counter += 1
                    return redirect('/sellorder/' + id + '/' + sym + '/' + str(price) + '/' + str(counter))
                else:
                    pass
            # investorder.validatestatus()
            # if investorder.get_status == "Successful":
            if 1 == 1:
                db = shelve.open('Investment.db', 'w')
                try:
                    history = db['UserHist']
                    sell = db['sell']
                    del db['UserHist']
                    del db['sell']
                    sell.append(investorder)
                    db['sell'] = sell
                    for i in history:
                        if i.get_userID() == userID:
                            i.add_history(investorder)
                        # else:
                        #     instance2 = DBI.TransactionHistory()
                        #     i.add_history(investorder)
                        #     history.append(instance2)
                    db['UserHist'] = history
                    db.close()
                    return redirect('/investhist')
                except Exception as e:
                    print(e)
                except:
                    print("Fail to open database")
                finally:
                    DBI.p2p()
    return render_template('Investment_OrderSell.html', id=id, sym=sym, price=price, counter=counter, form=sellorder, sstock=sstock)


@app.route('/investhist')
def investhist():
    # userID = 1
    userID = session.get('UserID')
    name = session.get('Name')
    db = shelve.open('Investment.db', 'r')
    try:
        history = db['UserHist']
    except Exception as e:
        print(e)
    db.close()
    for i in history:
        # if i.get_userID() == session['UserID']:
        if i.get_userID() == userID:
            thistory = i.get_transactionhistory()
            for j in thistory:
                j.check_expiry()
    return render_template('Investment_Transactionhistory.html', name=name, userID=userID, thistory=reversed(thistory))
# END OF INVESTMENT

# GIRO
class gForm(Form):
    bank = SelectField('Bank ', [validators.DataRequired()], choices=[("", "Select")])
    accNum = SelectField('Account Number ', [validators.DataRequired()], choices=[("", "Select")])
    insuranceName = SelectField('Policy Name ', [validators.DataRequired()], choices=[("", "Select")])

@app.route('/giro', methods=['GET', 'POST'])
def giro():
    form = gForm(request.form)
    giroList = []
    try:
        db = shelve.open('bank.db','r')
        accountList = db['banks']
        db.close()
    except Exception as e:
        print(e)
    try:
        db = shelve.open('insurance.db', 'r')
        insuranceList = db['Accounts']
        db.close()
    except Exception as e:
        print(e)
    try:
        db = shelve.open('insurance.db','r')
        ownedList = db['Accounts']
        db.close()
    except Exception as e:
        print(e)
    try:
        db = shelve.open('giro.db','r')
        giroList = db['Forms']
        db.close()
    except Exception as e:
        print(e)
    nameList = []
    try:
        for account in accountList:
            if account.get_userID() == session.get('UserID'):
                if account.get_accType() != "Current":
                    form.bank.choices.append((account.get_bankName() + " " + account.get_accType(), account.get_bankName() + " " + account.get_accType()))
                    if (str(account.get_accountNum()), str(account.get_accountNum())) not in form.accNum.choices:
                        form.accNum.choices.append((str(account.get_accountNum()), account.get_bankName() + " " + str(account.get_accountNum())))
    except:
        pass
    for policy in ownedList:
        if ownedList[policy].get_user() == session.get('UserID'):
            form.insuranceName.choices.append((ownedList[policy].get_policyName(), ownedList[policy].get_policyName()))

    if request.method == 'POST' and form.validate():
        user = session.get('UserID')
        g = giroForm.giro(user, form.bank.data, form.accNum.data, form.insuranceName.data)
        for insurance in insuranceList:
            if g.get_user() == session.get('UserID'):
                if insuranceList[insurance].get_user() == g.get_user():
                    if insuranceList[insurance].get_policyName() == g.get_giroName():
                        g.set_giroNum(insuranceList[insurance].get_policyNumber())
                        g.set_giroType(insuranceList[insurance].get_policyType())
                        g.set_giroAmt(insuranceList[insurance].get_monthlyPayment())
        gList = []
        try:
            db = shelve.open('giro.db', 'c')
            try:
                gList = db['Forms']
            except:
                pass
            gList.append(g)
            db['Forms'] = gList
            db.close()
        except Exception as e:
            print(e)
        return redirect(url_for('giroOwned'))

    ownedName = []
    try:
        for giro in giroList:
            ownedName.append(giro.get_giroName())
    except:
        pass

    try:
        for owned in ownedName:
            form.insuranceName.choices.remove((owned, owned))
    except:
        pass

    accForm = accountForm(request.form)
    accountList = {}
    list = []

    try:
        db = shelve.open('account.db', 'r')
        accountList = db['Accounts']
        db.close()
    except:
        print("Fail to open database")

    for account in accountList:
        list.append(accountList[account])

    # Account Details
    try:
        db = shelve.open('account.db', 'w')
        profileList = db['Accounts']
    except:
        print("Fail to open database")

    if request.method == 'POST' and form.validate():
        for profiles in profileList:
            if str(profileList[profiles].get_accountID()) == str(session.get('UserID')):
                profileList[profiles].set_bank(accForm.bank.data)
                profileList[profiles].set_phoneNum(accForm.phone.data)
    db['Accounts'] = profileList
    db.close()

    return render_template('giro.html', form=form, accountList=list, accountForm=accForm)

class giroFilter(Form):
    insurance_type = SelectField('', choices=[('', 'Select')], default='')

class giroDelete(Form):
    insuranceName = SelectField('', choices=[('', 'Select')], default='')

@app.route('/giroOwned', methods=['GET', 'POST'])
def giroOwned():
    filterForm = giroFilter(request.form)
    deleteForm = giroDelete(request.form)
    giroList = []
    if request.method == "POST":
        removeName = deleteForm.insuranceName.data
        try:
            db = shelve.open('giro.db', 'w')
            gList = db['Forms']
        except Exception as e:
            print(e)
        for g in gList:
            if g.get_user() == session.get('UserID'):
                if g.get_giroName() == removeName:
                    gList.remove(g)
        db['Forms'] = gList
        db.close()

    try:
        db = shelve.open('giro.db', 'r')
        giroList = db['Forms']
        db.close()
    except Exception as e:
        print(e)

    try:
        db = shelve.open('insurance.db', 'r')
        insuranceList = db['Accounts']
        db.close()
    except Exception as e:
        print(e)

    list = []

    searchType = filterForm.insurance_type.data
    for giro in giroList:
        if session.get('UserID') == giro.get_user():
            if giro.get_giroType() == searchType:
                list.append(giro)
            elif searchType == "":
                list.append(giro)
        elif session.get('UserID') == giro.get_user():
            if giro.get_giroType() == searchType:
                list.append(giro)
            elif searchType == "":
                list.append(giro)
    for giro in giroList:
        if giro.get_user() == session.get('UserID'):
            filterForm.insurance_type.choices.append((giro.get_giroType(), giro.get_giroType()))
            deleteForm.insuranceName.choices.append((giro.get_giroName(), giro.get_giroName()))

    try:
        db = shelve.open('bank.db','r')
        bankList = db['banks']
        db.close()
    except Exception as e:
        print(e)

    # for accounts in bankList:
    #     print(accounts.get_accountNum())

    # totalAmt = 0
    # for giro in giroList:
    #     print(giro.get_insuranceAmt())
    #     totalAmt += float(giro.get_insuranceAmt())
    # print(totalAmt)
    return render_template('giroOwned.html', list=list, filterForm=filterForm, deleteForm=deleteForm)
# END OF GIRO

# DATABASE DISPLAY
@app.route('/dbdisplay')
def dbdisplay():
    list = []
    list2 = []
    try:
        db = shelve.open('user.db','r')
        userList = db['Users']
        db.close()
        for key in userList:
            user = userList.get(key)
            list.append(user)
    except Exception as e:
        print(e)
    try:
        db = shelve.open('account.db','r')
        accountList = db['Accounts']
        db.close()
        for key in accountList:
            account = accountList.get(key)
            list2.append(account)
    except Exception as e:
        print(e)
    return render_template('databaseDisplay.html', users=list, accounts=list2)
# END OF DATABASE DISPLAY


if __name__ == '__main__':
    app.run()