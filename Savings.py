import shelve
from datetime import datetime


class Saving:
    savingid = 0

    def __init__(self, userID, type, goal, deadline, description):
        self.__class__.savingid += 1
        self.__savingID = self.__class__.savingid
        self.__userID = userID
        self.__type = type
        self.__goal = goal
        self.__deadline = deadline
        self.__description = description
        self.__savers = []
        self.__deposit_meth = ""
        self.__savedAmount = 0
        self.__lastUpdate = []
        self.__createdTime = datetime.now

    # GET METHODS
    def get_savingID(self):
        return self.__savingID

    def get_userID(self):
        return self.__userID

    def get_type(self):
        return self.__type

    def get_goal(self):
        return self.__goal

    def get_deadline(self):
        return self.__deadline
        # return datetime.strptime(self.get_deadline(), "%Y-%m-%d")

    def get_description(self):
        return self.__description

    def get_savers(self):
        return self.__savers

    def get_depositMeth(self):
        return self.__deposit_meth

    def get_savedAmount(self):
        return self.__savedAmount

    def get_lastUpdate(self):
        return self.__lastUpdate

    def get_percentage(self):
        return str(self.get_savedAmount() * 100 / self.get_goal())

        # SET METHODS

    def set_goal(self, goal):
        self.__goal = goal

    def set_deadline(self, deadline):
        self.__deadline = deadline

    def set_description(self, description):
        self.__description = description

    def set_savers(self, savers):
        savers = savers.split(",")
        for saver in savers:
            self.__savers.append(saver)

    def set_depositMeth(self, type, amount, accountNum):
        if type == "Manually":
            self.__deposit_meth = type
        else:
            self.__deposit_meth = type + "," + str(amount) + "," + str(accountNum)

    def set_savedAmount(self, amount):
        self.__savedAmount += amount

    def set_lastUpdate(self, date):
        self.__lastUpdate.append(date)


def static_instance():
    save1 = Saving(1, "Personal", 10000, "2019-06-01", "Studies")
    save1.set_savedAmount(200)
    save1.set_depositMeth("Auto", 500, "199-43333-3")
    save2 = Saving(2, "Group", 500, "2019-12-1", "Birthday Present")
    save2.set_savedAmount(50)
    save2.set_depositMeth("Manually", 0, 0)
    save2.set_savers("Zheng Jie 133-55555-3")

    save3 = Saving(2, "Group", 100000, "2019-06-01", "House")
    save3.set_savedAmount(2200)
    save3.set_depositMeth("Auto", 200, "199-44455-9")

    save4 = Saving(2, "Others", 7000, "2019-08-09", "Set aside")
    save4.set_savedAmount(1400)
    save4.set_depositMeth("Manually", 0, 0)

    try:
        savings = []
        db = shelve.open('savings.db', 'c')
        savings.extend([save1, save2, save3, save4])
        db['savings'] = savings
        db.close()
    except Exception as e:
        print(e)


static_instance()

