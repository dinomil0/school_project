import User
import shelve

class Account(User.User):
    accountNum = 1000000

    def __init__(self, nric, password, surname, name, birthdate, accountType, accountBalance):
        User.User.__init__(self, nric, password,)
        self.__class__.accountNum += 1
        self.__accountNumber = self.__class__.accountNum
        self.__surname = surname
        self.__name = name
        self.__birthdate = birthdate
        self.__bank = ""
        self.__accountType = accountType
        self.__accountBalance = 0
        self.set_accountBalance(accountBalance)

    def get_surname(self):
        return self.__surname

    def get_name(self):
        return self.__name

    def get_birthdate(self):
        return self.__birthdate

    def get_bank(self):
        return self.__bank

    def get_accountNumber(self):
        return self.__accountNumber

    def get_accountType(self):
        return self.__accountType

    def get_accountBalance(self):
        return self.__accountBalance

    def set_surname(self,surname):
        self.__surname = surname

    def set_name(self,name):
        self.__name = name

    def set_birthdate(self,birthdate):
        self.__birthdate = birthdate

    def set_bank(self,bank):
        self.__bank = bank

    def set_accountNumber(self,accountNumber):
        self.__accountNumber = accountNumber

    def set_accountType(self,accountType):
        self.__accountType = accountType

    def set_accountBalance(self,accountBalance):
        if int(accountBalance) >= 0:
            self.__accountBalance = accountBalance

a1 = Account("S1234567E", "password", "Tan", "John", "15/09/1995", "Savings", "5000")
a2 = Account("S2345678E", "password", "Lee", "Jerry", "10/06/1997", "Savings", "5000")
a3 = Account("S3456789E", "password", "Chan", "Joseph", "05/03/1999", "Current", "5000")
a1.set_bank("DBS")
a2.set_bank("UOB")
a3.set_bank("OCBC")

accountList = {}

accountList[a1.get_accountNumber()] = a1
accountList[a2.get_accountNumber()] = a2
accountList[a3.get_accountNumber()] = a3

try:
    db = shelve.open('account.db','c')
    db['Accounts'] = accountList
    db.close()
except:
    print('Fail to open database')