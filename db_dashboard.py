# class Inputs(Form):
#     myChoices = #number of choices
#     myField = SelectField(u'Field name', choices = myChoices, validators = [Required()])
import shelve
import datetime
import Bank

#DashBoard Tab
class Payments:
    def __init__(self, userID, months, transactions):
        self.__userID = userID
        self.__months = months
        self.__transactions = transactions

    def get_userID(self):
        return self.__userID

    def get_months(self):
        return self.__months

    def get_similar_transactions(self):
        return self.__similar_transactions

    # def add
    def set_userID(self, userID):
        self.__userID = userID

    def set_months(self, months):
        self.__months = months

    def set_transactios(self, transactions):
        for similar_transactions in transactions:
            print(similar_transactions)


class Budget:
    def __init__(self, userID, amount, merchant_code, month):
        self.__userID = userID
        self.__amount = amount
        self.__merchant_code = merchant_code
        self.__category = self.set_category()
        self.__deadline = ""
        self.__month = month

    def get_userID(self):
        return self.__userID

    def get_amount(self):
        return self.__amount

    def get_merchant_code(self):
        return self.__merchant_code

    def get_month(self):
        return self.__month

    def get_category(self):
        return self.__category

    def get_deadline(self):
        return self.__deadline

    def get_months(self):
        return self.__months

    def set_userID(self, UserID):
        self.__userID = UserID

    def set_merchant_code(self, merchant_code):
        self.__merchant_code = merchant_code

    def set_category(self):
        dict = shelve.open('mcc.db', 'r')
        for category_codes in dict:
            if category_codes == self.get_merchant_code():
                return dict[category_codes]

    def set_amount(self, amount):
        self.__amount = amount

    def set_deadline(self, deadline):
        self.__deadline = deadline

    def set_months(self, months):
        self.__months = months


#TransactionHist Tab
class Transaction_Hist:
    transID = 0

    def __init__(self, userID, date, merchant_code, description, type, amount, month, bank):
        self.__class__.transID +=1
        self.__transID = self.__class__.transID
        self.__userID = userID
        self.__date = date[0:10]
        self.__merchant_code = merchant_code
        self.__category = self.set_category()
        self.__description = description
        self.__amount = amount
        self.__month = month
        self.__bank = bank
        self.__type = type
        self.__balance = 0
        self.__numDate = ""
        self.set_numDate(date)

    def get_userID(self):
        return self.__userID

    def get_date(self):
        return self.__date

    def get_merchant_code(self):
        return self.__merchant_code

    def get_category(self):
        return self.__category

    def get_description(self):
        return self.__description

    def get_amount(self):
        return self.__amount

    def get_month(self):
        return self.__month

    def get_bank(self):
        return self.__bank

    def get_type(self):
        return self.__type

    def get_balance(self):
        return self.__balance

    # def get_deposit(self):
    #     self.__balance +=

    def get_numDate(self):
        return self.__numDate

    def get_categoriesmcc(self, merchant_code):
        if merchant_code == "Money Orders - Wire Transfer\n":
            return "4829"
        elif merchant_code == "Electric, Gas, Sanitary and Water Utilities\n":
            return "4900"
        elif merchant_code == "Motor vehicle supplies\n":
            return "5013"
        elif merchant_code == "Computers, Computer Equipment, Software\n":
            return "5045"
        elif merchant_code == "Watch, Clock, Jewelry, and Silverware Stores\n":
            return "5944"
        elif merchant_code == "Department Stores\n":
            return "5311"
        elif merchant_code == "Supermarkets\n":
            return "5411"
        elif merchant_code == "Eating places and Restaurants\n":
            return "5812"
        elif merchant_code == "Drinking Places(Bars and Nightclubs)\n":
            return "5813"
        elif merchant_code == "Fast Food Restaurants\n":
            return "5814"
        elif merchant_code == "Sporting Goods Stores\n":
            return "5941"
        elif merchant_code == "Book Stores\n":
            return "5942"
        elif merchant_code == "Insurance Premiums\n":
            return "6381"
        elif merchant_code == "Counseling Service - Debt, Marriage, Personal\n":
            return "7277"
        elif merchant_code == "Membership Clubs\n":
            return "7997"
        elif merchant_code == "Tax Payments\n":
            return "9311"
        elif merchant_code == "Medical and Dental Laboratories\n":
            return "8071"
        elif merchant_code == "Duty Free Store\n":
            return "5309"


    def get_IntMonth(self):
        if self.get_month() == "Jan":
            return 13
        elif self.get_month() == "Feb":
            return 2
        elif self.get_month() == "Mar":
            return 3
        elif self.get_month() == "Apr":
            return 4
        elif self.get_month() == "May":
            return 5
        elif self.get_month() == "Jun":
            return 6
        elif self.get_month() == "Jul":
            return 7
        elif self.get_month() == "Aug":
            return 8
        elif self.get_month() == "Sep":
            return 9
        elif self.get_month() == "Oct":
            return 10
        elif self.get_month() == "Nov":
            return 11
        elif self.get_month() == "Dec":
            return 12
        else:
            return "Wrong month"

    def get_StrMonth(self):
        if self.get_month() == 13:
            return "Jan"
        elif self.get_month() == 2:
            return "Feb"
        elif self.get_month() == 3:
            return "Mar"
        elif self.get_month() == 4:
            return "Apr"
        elif self.get_month() == 5:
            return "May"
        elif self.get_month() == 6:
            return "Jun"
        elif self.get_month() == 7:
            return "Jul"
        elif self.get_month() == 8:
            return "Aug"
        elif self.get_month() == 9:
            return "Sep"
        elif self.get_month() == 10:
            return "Oct"
        elif self.get_month() == 11:
            return "Nov"
        elif self.get_month() == 12:
            return "Dec"
        else:
            return "Invalid integer"

    def set_date(self, date):
        self.__date = date

    def set_merchant_code(self, merchant_code):
        try:
            int(merchant_code)
        except ValueError:
            return self.get_categoriesmcc(merchant_code)

    def set_category(self):
        dict = shelve.open('mcc.db', 'r')
        for category_codes in dict:
            if category_codes == self.get_merchant_code():
                return dict[category_codes]

        dict.close()

    def set_description(self, description):
        self.__description = description

    def set_amount(self, amount):
        self.__amount = float(amount)

    def set_type(self, type):
        self.__type = type

    def set_balance(self, balance):
        # if self.__type == "Withdrawal":
        #     self.__balance = self.__amount + balance
        #     return self.__balance
        # elif self.__type == "Deposit":
        #     self.__balance = self.__amount - balance
        #     return self.__balance
        self.__balance = self.__amount + balance

    def set_numDate(self, date):
        self.__numDate = date[8:10]

def static_data():
    # DBS Transactions USER 1
    transaction = []
    # November
    t1 = Transaction_Hist(1, '2018/11/01', '5812', 'Foogle NYP', 'Withdrawal', 6.00, 'Nov', 'DBS')
    t2 = Transaction_Hist(1, '2018/11/01', '5411', 'NTUC', 'Withdrawal', 25.00, 'Nov', 'DBS')
    t3 = Transaction_Hist(1, '2018/11/02', '4829', 'AMK Hub', 'Deposit', 500.00, 'Nov', 'DBS')
    t4 = Transaction_Hist(1, '2018/11/03', '5814', 'KFC Junction 8', 'Withdrawal', 8.50, 'Nov', 'DBS')
    t5 = Transaction_Hist(1, '2018/11/04', '5941', 'SportsLink AMK', 'Withdrawal', 40.00, 'Nov', 'DBS')
    t6 = Transaction_Hist(1, '2018/11/05', '5812', 'Sushi Express NEX', 'Withdrawal', 50.00, 'Nov', 'DBS')
    t7 = Transaction_Hist(1, '2018/11/10', '5309', 'Changi Aiport', 'Withdrawal', 150.00, 'Nov', 'DBS')
    t8 = Transaction_Hist(1, '2018/11/11', '6381', 'Monthly PRUactive saver', 'Withdrawal', 25.00, 'Nov', 'DBS')
    t9 = Transaction_Hist(1, '2018/11/13', '6381', 'Monthly AIA PLATINUM PRO SECURE', 'Withdrawal', 15.00, 'Nov', 'DBS')
    t10 = Transaction_Hist(1, '2018/11/15', '5411', 'Cold Storage', 'Withdrawal', 35.00, 'Nov', 'DBS')
    t11 = Transaction_Hist(1, '2018/11/17', '5814', 'Macs AMK', 'Withdrawal', 10.00, 'Nov', 'DBS')
    t12 = Transaction_Hist(1, '2018/11/19', '5814', 'Subway AMK', 'Withdrawal', 8.00, 'Nov', 'DBS')
    t13 = Transaction_Hist(1, '2018/11/20', '5812', 'Swensens AMK', 'Withdrawal', 85.00, 'Nov', 'DBS')
    t14 = Transaction_Hist(1, '2018/11/20', '5411', 'NTUC', 'Withdrawal', 23.00, 'Nov', 'DBS')
    t15 = Transaction_Hist(1, '2018/11/21', '7997', 'Fitness First Paragon', 'Withdrawal', 150.00, 'Nov', 'DBS')
    t16 = Transaction_Hist(1, '2018/11/23', '4829', 'Junction 8', 'Deposit', 350.00, 'Nov', 'DBS')
    t17 = Transaction_Hist(1, '2018/11/25', '9311', 'Property Tax', 'Withdrawal', 250.00, 'Nov', 'DBS')
    t18 = Transaction_Hist(1, '2018/11/28', '4829', 'Salary', 'Deposit', 3000.00, 'Nov', 'DBS')
    t19 = Transaction_Hist(1, '2018/11/29', '5813', 'Holland Village 3 Crowns', 'Withdrawal', 50.00, 'Nov', 'DBS')
    t20 = Transaction_Hist(1, '2018/11/30', '5942', 'NYP Popular', 'Withdrawal', 5.30, 'Nov', 'DBS')
    t21 = Transaction_Hist(1, '2018/11/30', '8071', 'NUH Dental', 'Withdrawal', 150.00, 'Nov', 'DBS')
    t22 = Transaction_Hist(1, '2018/11/30', '5311', 'Takashimaya Singapore', 'Withdrawal', 120.00, 'Nov', 'DBS')
    #
    # # December
    f1 = Transaction_Hist(1, '2018/12/01', '5411', 'Cold Storage', 'Withdrawal', 16.00, 'Dec', 'DBS')
    f2 = Transaction_Hist(1, '2018/12/01', '5942', 'NYP Popular', 'Withdrawal', 6.00, 'Dec', 'DBS')
    f3 = Transaction_Hist(1, '2018/12/03', '5814', 'Macs AMK', 'Withdrawal', 8.50, 'Dec', 'DBS')
    f4 = Transaction_Hist(1, '2018/12/04', '5814', 'Subway AMK', 'Withdrawal', 20.00, 'Dec', 'DBS')
    f5 = Transaction_Hist(1, '2018/12/05', '5812', 'Sushi Express NEX', 'Withdrawal', 80.00, 'Dec', 'DBS')
    f6 = Transaction_Hist(1, '2018/12/10', '5411', 'NTUC', 'Withdrawal', 15.00, 'Dec', 'DBS')
    f7 = Transaction_Hist(1, '2018/12/11', '6381', 'Monthly PRUactive saver', 'Withdrawal', 25.00, 'Dec', 'DBS')
    f8 = Transaction_Hist(1, '2018/12/13', '6381', 'Monthly AIA PLATINUM PRO SECURE', 'Withdrawal', 15.00, 'Dec', 'DBS')
    f9 = Transaction_Hist(1, '2018/12/15', '5812', 'Ramen Keisuke Lobster King', 'Withdrawal', 15.00, 'Dec', 'DBS')
    f10 = Transaction_Hist(1, '2018/12/17', '5814', 'Ichiban AMK', 'Withdrawal', 12.00, 'Dec', 'DBS')
    f11 = Transaction_Hist(1, '2018/12/19', '5814', 'Ajisen Ramen Dining AMK', 'Withdrawal', 21.00, 'Dec', 'DBS')
    f12 = Transaction_Hist(1, '2018/12/20', '5812', 'Cathay Cineplexes AMK', 'Withdrawal', 10.00, 'Dec', 'DBS')
    f13 = Transaction_Hist(1, '2018/12/20', '5411', 'NTUC Fairprice', 'Withdrawal', 23.00, 'Dec', 'DBS')
    f14 = Transaction_Hist(1, '2018/12/22', '7997', 'Fitness First Paragon', 'Withdrawal', 150.00, 'Dec', 'DBS')
    f15 = Transaction_Hist(1, '2018/12/25', '9311', 'Property Tax', 'Withdrawal', 250.00, 'Dec', 'DBS')
    f16 = Transaction_Hist(1, '2018/12/25', '5812', 'Food Junction NEX', 'Withdrawal', 9.00, 'Dec', 'DBS')
    f17 = Transaction_Hist(1, '2018/12/28', '4829', 'Salary', 'Deposit', 3000.00, 'Dec', 'DBS')

    #Jan
    m1 = Transaction_Hist(1, '2019/01/01', '5311', 'Takashimaya Singapore', 'Withdrawal', 106.00, 'Jan', 'DBS')
    m2 = Transaction_Hist(1, '2019/01/03', '5411', 'NTUC', 'Withdrawal', 25.00, 'Jan', 'DBS')
    m3 = Transaction_Hist(1, '2019/01/04', '5812', 'Sushi Express NEX', 'Withdrawal', 90.00, 'Jan', 'DBS')
    m4 = Transaction_Hist(1, '2019/01/04', '5814', 'KFC Junction 8', 'Withdrawal', 15.50, 'Jan', 'DBS')
    m5 = Transaction_Hist(1, '2019/01/04', '5814', 'Macs AMK', 'Withdrawal', 40.00, 'Jan', 'DBS')
    m6 = Transaction_Hist(1, '2019/01/09', '5309', 'Changi Aiport', 'Withdrawal', 58.00, 'Jan', 'DBS')
    m7 = Transaction_Hist(1, '2019/01/09', '5309', 'Changi Aiport', 'Withdrawal', 120.00, 'Jan', 'DBS')
    m8 = Transaction_Hist(1, '2019/01/11', '6381', 'Monthly PRUactive saver', 'Withdrawal', 25.00, 'Jan', 'DBS')
    m9 = Transaction_Hist(1, '2019/01/13', '6381', 'Monthly AIA PLATINUM PRO SECURE', 'Withdrawal', 15.00, 'Jan', 'DBS')
    m10 = Transaction_Hist(1, '2019/01/15', '5814', 'KFC Junction 8', 'Withdrawal', 40.00, 'Jan', 'DBS')
    m11 = Transaction_Hist(1, '2019/01/17', '5814', 'Macs AMK', 'Withdrawal', 15.00, 'Jan', 'DBS')
    m12 = Transaction_Hist(1, '2019/01/18', '5812', 'Swensens AMK', 'Withdrawal', 28.00, 'Jan', 'DBS')
    m13 = Transaction_Hist(1, '2019/01/18', '5411', 'Cold Storage', 'Withdrawal', 85.00, 'Jan', 'DBS')
    m14 = Transaction_Hist(1, '2019/01/20', '5411', 'NTUC', 'Withdrawal', 23.00, 'Jan', 'DBS')
    m15 = Transaction_Hist(1, '2019/01/21', '7997', 'Fitness First Paragon', 'Withdrawal', 150.00, 'Jan', 'DBS')
    m16 = Transaction_Hist(1, '2019/01/25', '9311', 'Property Tax', 'Withdrawal', 250.00, 'Jan', 'DBS')
    m17 = Transaction_Hist(1, '2019/01/25', '5812', 'Din Tai Fung NEX', 'Withdrawal', 95.20, 'Jan', 'DBS')
    m18 = Transaction_Hist(1, '2019/01/28', '4829', 'Salary', 'Deposit', 3000.00, 'Jan', 'DBS')
    m19 = Transaction_Hist(1, '2019/01/28', '5813', 'Molly Malones', 'Withdrawal', 50.00, 'Jan', 'DBS')
    m20 = Transaction_Hist(1, '2019/01/28', '5942', 'Popular', 'Withdrawal', 15.30, 'Jan', 'DBS')

    # UOB
    # November
    a1 = Transaction_Hist(1, '2018/11/01', '5812', 'Foogle NYP', 'Withdrawal', 16.00, 'Nov', 'UOB')
    a2 = Transaction_Hist(1, '2018/11/01', '5411', 'NTUC', 'Withdrawal', 5.00, 'Nov', 'UOB')
    a3 = Transaction_Hist(1, '2018/11/02', '4829', 'AMK Hub', 'Deposit', 500.00, 'Nov', 'UOB')
    a4 = Transaction_Hist(1, '2018/11/03', '5814', 'KFC Junction 8', 'Withdrawal', 18.50, 'Nov', 'UOB')
    a5 = Transaction_Hist(1, '2018/11/04', '5941', 'SportsLink AMK', 'Withdrawal', 20.00, 'Nov', 'UOB')
    a6 = Transaction_Hist(1, '2018/11/05', '5812', 'Sushi Express NEX', 'Withdrawal', 65.00, 'Nov', 'UOB')
    a7 = Transaction_Hist(1, '2018/11/10', '5309', 'Changi Aiport', 'Withdrawal', 120.00, 'Nov', 'UOB')
    a8 = Transaction_Hist(1, '2018/11/11', '4900', 'Utilities Bill', 'Withdrawal', 125.00, 'Nov', 'UOB')
    a9 = Transaction_Hist(1, '2018/11/15', '7277', 'Rent Debt', 'Withdrawal', 300.00, 'Nov', 'UOB')
    a10 = Transaction_Hist(1, '2018/11/15', '5411', 'Cold Storage', 'Withdrawal', 40.00, 'Nov', 'UOB')
    a11 = Transaction_Hist(1, '2018/11/17', '5814', 'Macs AMK', 'Withdrawal', 15.00, 'Nov', 'UOB')
    a12 = Transaction_Hist(1, '2018/11/19', '5814', 'Subway AMK', 'Withdrawal', 18.00, 'Nov', 'UOB')
    a13 = Transaction_Hist(1, '2018/11/20', '5812', 'Swensens AMK', 'Withdrawal', 40.00, 'Nov', 'UOB')
    a14 = Transaction_Hist(1, '2018/11/20', '5411', 'NTUC', 'Withdrawal', 15.00, 'Nov', 'UOB')
    a15 = Transaction_Hist(1, '2018/11/21', '7997', 'Fitness First Paragon', 'Withdrawal', 32.00, 'Nov', 'UOB')
    a16 = Transaction_Hist(1, '2018/11/23', '4829', 'Junction 8', 'Deposit', 350.00, 'Nov', 'UOB')
    a17 = Transaction_Hist(1, '2018/11/25', '5812', 'Food Junction NEX', 'Withdrawal', 25.00, 'Nov', 'UOB')
    a18 = Transaction_Hist(1, '2018/11/28', '4829', 'Salary', 'Deposit', 3000.00, 'Nov', 'UOB')
    a19 = Transaction_Hist(1, '2018/11/29', '5813', 'Holland Village 3 Crowns', 'Withdrawal', 42.00, 'Nov', 'UOB')
    a20 = Transaction_Hist(1, '2018/11/30', '5942', 'NYP Popular', 'Withdrawal', 10.50, 'Nov', 'UOB')
    a21 = Transaction_Hist(1, '2018/11/30', '8071', 'NUH Dental', 'Withdrawal', 65.40, 'Nov', 'UOB')

    #
    # # December
    b1 = Transaction_Hist(1, '2018/12/01', '5411', 'Cold Storage', 'Withdrawal', 15.00, 'Dec', 'UOB')
    b2 = Transaction_Hist(1, '2018/12/01', '5942', 'NYP Popular', 'Withdrawal', 3.20, 'Dec', 'UOB')
    b3 = Transaction_Hist(1, '2018/12/03', '5814', 'Macs AMK', 'Withdrawal', 9.50, 'Dec', 'UOB')
    b4 = Transaction_Hist(1, '2018/12/04', '5814', 'Subway AMK', 'Withdrawal', 10.00, 'Dec', 'UOB')
    b5 = Transaction_Hist(1, '2018/12/05', '5812', 'Sushi Express NEX', 'Withdrawal', 90.00, 'Dec', 'UOB')
    b6 = Transaction_Hist(1, '2018/12/10', '5411', 'NTUC', 'Withdrawal', 15.00, 'Dec', 'UOB')
    b7 = Transaction_Hist(1, '2018/12/11', '4900', 'Utilities Bill', 'Withdrawal', 125.00, 'Dec', 'UOB')
    b8 = Transaction_Hist(1, '2018/12/15', '7277', 'Rent Debt', 'Withdrawal', 300.00, 'Dec', 'UOB')
    b9 = Transaction_Hist(1, '2018/12/15', '5812', 'Ramen Keisuke Lobster King', 'Withdrawal', 15.00, 'Dec', 'UOB')
    b10 = Transaction_Hist(1, '2018/12/17', '5814', 'Ichiban AMK', 'Withdrawal', 14.00, 'Dec', 'UOB')
    b11 = Transaction_Hist(1, '2018/12/19', '5814', 'Ajisen Ramen Dining AMK', 'Withdrawal', 21.00, 'Dec', 'UOB')
    b12 = Transaction_Hist(1, '2018/12/20', '5812', 'Cathay Cineplexes AMK', 'Withdrawal', 25.00, 'Dec', 'UOB')
    b13 = Transaction_Hist(1, '2018/12/20', '5411', 'NTUC Fairprice', 'Withdrawal', 23.00, 'Dec', 'UOB')
    b14 = Transaction_Hist(1, '2018/12/22', '7997', 'Fitness First Paragon', 'Withdrawal', 150.00, 'Dec', 'UOB')
    b15 = Transaction_Hist(1, '2018/12/25', '4829', 'Junction 8', 'Deposit', 350.00, 'Dec', 'UOB')
    b16 = Transaction_Hist(1, '2018/12/25', '5812', 'Food Junction NEX', 'Withdrawal', 9.00, 'Dec', 'UOB')
    b17 = Transaction_Hist(1, '2018/12/28', '4829', 'Salary', 'Deposit', 3000.00, 'Dec', 'UOB')

    #January
    c1 = Transaction_Hist(1, '2019/01/01', '5311', 'Takashimaya Singapore', 'Withdrawal', 126.00, 'Jan', 'UOB')
    c2 = Transaction_Hist(1, '2019/01/03', '5411', 'NTUC', 'Withdrawal', 15.00, 'Jan', 'UOB')
    c3 = Transaction_Hist(1, '2019/01/04', '5812', 'Sushi Express NEX', 'Withdrawal', 54.00, 'Jan', 'UOB')
    c4 = Transaction_Hist(1, '2019/01/04', '5814', 'KFC Junction 8', 'Withdrawal', 21.50, 'Jan', 'UOB')
    c5 = Transaction_Hist(1, '2019/01/04', '5814', 'Macs AMK', 'Withdrawal', 50.00, 'Jan', 'UOB')
    c6 = Transaction_Hist(1, '2019/01/09', '5309', 'Changi Aiport', 'Withdrawal', 52.00, 'Jan', 'UOB')
    c7 = Transaction_Hist(1, '2019/01/09', '5309', 'Changi Aiport', 'Withdrawal', 31.00, 'Jan', 'UOB')
    c8 = Transaction_Hist(1, '2019/01/11', '4900', 'Utilities Bill', 'Withdrawal', 125.00, 'Jan', 'UOB')
    c9 = Transaction_Hist(1, '2018/01/15', '7277', 'Rent Debt', 'Withdrawal', 300.00, 'Jan', 'UOB')
    c10 = Transaction_Hist(1, '2019/01/15', '5814', 'KFC Junction 8', 'Withdrawal', 40.00, 'Jan', 'UOB')
    c11 = Transaction_Hist(1, '2019/01/17', '5814', 'Macs AMK', 'Withdrawal', 15.00, 'Jan', 'UOB')
    c12 = Transaction_Hist(1, '2019/01/18', '5812', 'Swensens AMK', 'Withdrawal', 34.00, 'Jan', 'UOB')
    c13 = Transaction_Hist(1, '2019/01/18', '5411', 'Cold Storage', 'Withdrawal', 85.00, 'Jan', 'UOB')
    c14 = Transaction_Hist(1, '2019/01/20', '5411', 'NTUC', 'Withdrawal', 61.20, 'Jan', 'UOB')
    c15 = Transaction_Hist(1, '2019/01/21', '7997', 'Fitness First Paragon', 'Withdrawal', 150.00, 'Jan', 'UOB')
    c16 = Transaction_Hist(1, '2019/01/23', '8071', 'Coast Dental', 'Withdrawal', 150.00, 'Jan', 'UOB')
    c17 = Transaction_Hist(1, '2019/01/25', '5812', 'Din Tai Fung NEX', 'Withdrawal', 95.20, 'Jan', 'UOB')
    c18 = Transaction_Hist(1, '2019/01/28', '4829', 'Salary', 'Deposit', 3000.00, 'Jan', 'UOB')
    c19 = Transaction_Hist(1, '2019/01/28', '5813', 'Molly Malones', 'Withdrawal', 78.40, 'Jan', 'UOB')
    c20 = Transaction_Hist(1, '2019/01/28', '5942', 'Popular', 'Withdrawal', 15.30, 'Jan', 'UOB')
     #Feb
    z1 = Transaction_Hist(1, '2019/02/01', '0000', 'Nothing', 'Withdrawal', 0, 'Feb', 'UOB')

    #Feb
    e1 = Transaction_Hist(1, '2019/02/01', '0000', 'Nothing', 'Withdrawal', 0, 'Feb', 'DBS')
    # OCBC Transactions USER 2
    # November

    d1 = Transaction_Hist(2, '2018/11/01', '5812', 'Foogle NYP', 'Withdrawal', 6.00, 'Nov', 'OCBC')
    d2 = Transaction_Hist(2, '2018/11/01', '5411', 'NTUC', 'Withdrawal', 25.00, 'Nov', 'OCBC')
    d3 = Transaction_Hist(2, '2018/11/02', '4829', 'AMK Hub', 'Deposit', 500.00, 'Nov', 'OCBC')
    d4 = Transaction_Hist(2, '2018/11/03', '5814', 'KFC Junction 8', 'Withdrawal', 8.50, 'Nov', 'OCBC')
    d5 = Transaction_Hist(2, '2018/11/04', '5941', 'SportsLink AMK', 'Withdrawal', 40.00, 'Nov', 'OCBC')
    d6 = Transaction_Hist(2, '2018/11/05', '5812', 'Sushi Express NEX', 'Withdrawal', 50.00, 'Nov', 'OCBC')
    d7 = Transaction_Hist(2, '2018/11/10', '5309', 'Changi Aiport', 'Withdrawal', 150.00, 'Nov', 'OCBC')
    d8 = Transaction_Hist(2, '2018/11/11', '6381', 'Monthly PRUactive saver', 'Withdrawal', 25.00, 'Nov', 'OCBC')
    d9 = Transaction_Hist(2, '2018/11/13', '6381', 'Monthly AIA PLATINUM PRO SECURE', 'Withdrawal', 15.00, 'Jan', 'OCBC')
    d10 = Transaction_Hist(2, '2018/11/15', '5411', 'Cold Storage', 'Withdrawal', 35.00, 'Nov', 'OCBC')
    d11 = Transaction_Hist(2, '2018/11/17', '5814', 'Macs AMK', 'Withdrawal', 10.00, 'Nov', 'OCBC')
    d12 = Transaction_Hist(2, '2018/11/19', '5814', 'Subway AMK', 'Withdrawal', 8.00, 'Nov', 'OCBC')
    d13 = Transaction_Hist(2, '2018/11/20', '5812', 'Swensens AMK', 'Withdrawal', 85.00, 'Nov', 'OCBC')
    d14 = Transaction_Hist(2, '2018/11/20', '5411', 'NTUC', 'Withdrawal', 23.00, 'Nov', 'OCBC')
    d15 = Transaction_Hist(2, '2018/11/21', '7997', 'Fitness First Paragon', 'Withdrawal', 150.00, 'Nov', 'OCBC')
    d16 = Transaction_Hist(2, '2018/11/23', '4829', 'Junction 8', 'Deposit', 350.00, 'Nov', 'OCBC')
    d17 = Transaction_Hist(2, '2018/11/25', '5812', 'Food Junction NEX', 'Withdrawal', 15.00, 'Nov', 'OCBC')
    d18 = Transaction_Hist(2, '2018/11/28', '4829', 'Salary', 'Deposit', 3000.00, 'Nov', 'OCBC')
    d19 = Transaction_Hist(2, '2018/11/29', '5813', 'Holland Village 3 Crowns', 'Withdrawal', 50.00, 'Nov', 'OCBC')
    d20 = Transaction_Hist(2, '2018/11/30', '5942', 'NYP Popular', 'Withdrawal', 5.30, 'Nov', 'OCBC')
    d21 = Transaction_Hist(2, '2018/11/30', '8071', 'NUH Dental', 'Withdrawal', 150.00, 'Nov', 'OCBC')
    d22 = Transaction_Hist(2, '2018/11/31', '5311', 'Takashimaya Singapore', 'Withdrawal', 120.00, 'Nov', 'OCBC')
    # December
    g1 = Transaction_Hist(2, '2018/12/01', '5411', 'Cold Storage', 'Withdrawal', 18.00, 'Dec', 'OCBC')
    g2 = Transaction_Hist(2, '2018/12/01', '5942', 'NYP Popular', 'Withdrawal', 31.20, 'Dec', 'OCBC')
    g3 = Transaction_Hist(2, '2018/12/03', '5814', 'Macs AMK', 'Withdrawal', 5.50, 'Dec', 'OCBC')
    g4 = Transaction_Hist(2, '2018/12/04', '5814', 'Subway AMK', 'Withdrawal', 7.00, 'Dec', 'OCBC')
    g5 = Transaction_Hist(2, '2018/12/05', '5812', 'Sushi Express NEX', 'Withdrawal', 60.00, 'Dec', 'OCBC')
    g6 = Transaction_Hist(2, '2018/12/10', '5411', 'NTUC', 'Withdrawal', 15.00, 'Dec', 'OCBC')
    g7 = Transaction_Hist(2, '2018/11/11', '6381', 'Monthly PRUactive saver', 'Withdrawal', 25.00, 'Dec', 'OCBC')
    g8 = Transaction_Hist(2, '2018/11/13', '6381', 'Monthly AIA PLATINUM PRO SECURE', 'Withdrawal', 15.00, 'Dec', 'OCBC')
    g9 = Transaction_Hist(2, '2018/12/15', '5812', 'Ramen Keisuke Lobster King', 'Withdrawal', 18.00, 'Dec', 'OCBC')
    g10 = Transaction_Hist(2, '2018/12/17', '5814', 'Ichiban AMK', 'Withdrawal', 14.00, 'Dec', 'OCBC')
    g11 = Transaction_Hist(2, '2018/12/19', '5814', 'Ajisen Ramen Dining AMK', 'Withdrawal', 26.00, 'Dec', 'OCBC')
    g12 = Transaction_Hist(2, '2018/12/20', '5812', 'Cathay Cineplexes AMK', 'Withdrawal', 25.00, 'Dec', 'OCBC')
    g13 = Transaction_Hist(2, '2018/12/20', '5411', 'NTUC Fairprice', 'Withdrawal', 29.00, 'Dec', 'OCBC')
    g14 = Transaction_Hist(2, '2018/12/22', '7997', 'Fitness First Paragon', 'Withdrawal', 150.00, 'Dec', 'OCBC')
    g15 = Transaction_Hist(2, '2018/12/25', '4829', 'Junction 8', 'Deposit', 40.00, 'Dec', 'OCBC')
    g16 = Transaction_Hist(2, '2018/12/25', '5812', 'Food Junction NEX', 'Withdrawal', 29.00, 'Dec', 'OCBC')
    g17 = Transaction_Hist(2, '2018/12/28', '4829', 'Salary', 'Deposit', 3000.00, 'Dec', 'OCBC')

    # Jan
    h1 = Transaction_Hist(2, '2019/01/01', '5311', 'Takashimaya Singapore', 'Withdrawal', 106.00, 'Jan', 'OCBC')
    h2 = Transaction_Hist(2, '2019/01/03', '5411', 'NTUC', 'Withdrawal', 25.00, 'Jan', 'OCBC')
    h3 = Transaction_Hist(2, '2019/01/04', '5812', 'Sushi Express NEX', 'Withdrawal', 90.00, 'Jan', 'OCBC')
    h4 = Transaction_Hist(2, '2019/01/04', '5814', 'KFC Junction 8', 'Withdrawal', 15.50, 'Jan', 'OCBC')
    h5 = Transaction_Hist(2, '2019/01/04', '5814', 'Macs AMK', 'Withdrawal', 40.00, 'Jan', 'OCBC')
    h6 = Transaction_Hist(2, '2019/01/09', '5309', 'Changi Aiport', 'Withdrawal', 58.00, 'Jan', 'OCBC')
    h7 = Transaction_Hist(2, '2019/01/09', '5309', 'Changi Aiport', 'Withdrawal', 120.00, 'Jan', 'OCBC')
    h8 = Transaction_Hist(2, '2019/01/11', '6381', 'Monthly PRUactive saver', 'Withdrawal', 25.00, 'Jan', 'OCBC')
    h9 = Transaction_Hist(2, '2019/01/13', '6381', 'Monthly AIA PLATINUM PRO SECURE', 'Withdrawal', 15.00, 'Jan', 'OCBC')
    h10 = Transaction_Hist(2, '2019/01/15', '5814', 'KFC Junction 8', 'Withdrawal', 40.00, 'Jan', 'OCBC')
    h11 = Transaction_Hist(2, '2019/01/17', '5814', 'Macs AMK', 'Withdrawal', 15.00, 'Jan', 'OCBC')
    h12 = Transaction_Hist(2, '2019/01/18', '5812', 'Swensens AMK', 'Withdrawal', 28.00, 'Jan', 'OCBC')
    h13 = Transaction_Hist(2, '2019/01/18', '5411', 'Cold Storage', 'Withdrawal', 85.00, 'Jan', 'OCBC')
    h14 = Transaction_Hist(2, '2019/01/20', '5411', 'NTUC', 'Withdrawal', 23.00, 'Jan', 'OCBC')
    h15 = Transaction_Hist(2, '2019/01/21', '7997', 'Fitness First Paragon', 'Withdrawal', 150.00, 'Jan', 'OCBC')
    h16 = Transaction_Hist(2, '2019/01/25', '9311', 'Property Tax', 'Withdrawal', 250.00, 'Jan', 'OCBC')
    h17 = Transaction_Hist(2, '2019/01/25', '5812', 'Din Tai Fung NEX', 'Withdrawal', 95.20, 'Jan', 'OCBC')
    h18 = Transaction_Hist(2, '2019/01/28', '4829', 'Salary', 'Deposit', 3000.00, 'Jan', 'OCBC')
    h19 = Transaction_Hist(2, '2019/01/28', '5813', 'Molly Malones', 'Withdrawal', 50.00, 'Jan', 'OCBC')
    h20 = Transaction_Hist(2, '2019/01/28', '5942', 'Popular', 'Withdrawal', 15.30, 'Jan', 'OCBC')

    # Nov
    i1 = Transaction_Hist(2, '2018/11/01', '5812', 'Foogle NYP', 'Withdrawal', 16.00, 'Nov', 'UOB')
    i2 = Transaction_Hist(2, '2018/11/01', '5411', 'NTUC', 'Withdrawal', 20.00, 'Nov', 'UOB')
    i3 = Transaction_Hist(2, '2018/11/12', '4829', 'AMK Hub', 'Deposit', 50.00, 'Nov', 'UOB')
    i4 = Transaction_Hist(2, '2018/11/21', '5814', 'KFC Junction 8', 'Withdrawal', 18.50, 'Nov', 'UOB')
    i5 = Transaction_Hist(2, '2018/11/23', '5941', 'SportsLink AMK', 'Withdrawal', 40.00, 'Nov', 'UOB')
    i6 = Transaction_Hist(2, '2018/11/28', '5812', 'Sushi Express NEX', 'Withdrawal', 50.00, 'Nov', 'UOB')

    # December
    j1 = Transaction_Hist(2, '2018/12/01', '5411', 'Cold Storage', 'Withdrawal', 108.00, 'Dec', 'UOB')
    j2 = Transaction_Hist(2, '2018/12/03', '5942', 'NYP Popular', 'Withdrawal', 31.20, 'Dec', 'UOB')
    j3 = Transaction_Hist(2, '2018/12/10', '5814', 'Macs AMK', 'Withdrawal', 15.50, 'Dec', 'UOB')
    j4 = Transaction_Hist(2, '2018/12/12', '5814', 'Subway AMK', 'Withdrawal', 7.00, 'Dec', 'UOB')
    j5 = Transaction_Hist(2, '2018/12/13', '5812', 'Sushi Express NEX', 'Withdrawal', 60.00, 'Dec', 'UOB')
    j6 = Transaction_Hist(2, '2018/12/18', '5411', 'NTUC', 'Withdrawal', 115.00, 'Dec', 'UOB')
    j7 = Transaction_Hist(2, '2018/11/23', '6381', 'Monthly PRUactive saver', 'Withdrawal', 25.00, 'Nov', 'UOB')

    # Jan
    k1 = Transaction_Hist(2, '2019/01/01', '5311', 'Takashimaya Singapore', 'Withdrawal', 126.00, 'Jan', 'UOB')
    k2 = Transaction_Hist(2, '2019/01/03', '5411', 'NTUC', 'Withdrawal', 25.00, 'Jan', 'UOB')
    k3 = Transaction_Hist(2, '2019/01/08', '5812', 'Sushi Express NEX', 'Withdrawal', 50.00, 'Jan', 'UOB')
    k4 = Transaction_Hist(2, '2019/01/12', '5814', 'KFC Junction 8', 'Withdrawal', 15.50, 'Jan', 'UOB')
    k5 = Transaction_Hist(2, '2019/01/14', '5814', 'Macs AMK', 'Withdrawal', 40.00, 'Jan', 'UOB')
    k6 = Transaction_Hist(2, '2019/01/15', '5309', 'Changi Aiport', 'Withdrawal', 58.00, 'Jan', 'UOB')
    k7 = Transaction_Hist(2, '2019/01/21', '5309', 'Changi Aiport', 'Withdrawal', 120.00, 'Jan', 'UOB')

    try:
        db = shelve.open('transaction.db', 'c')
        transaction.extend([t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15, t16, t17, t18, t19, t20, t21, t22])
        transaction.extend([f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14, f15, f16, f17])
        transaction.extend([m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, m13, m14, m15, m16, m17, m18, m19, m20])
        transaction.extend([a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21])
        transaction.extend([b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15, b16, b17])
        transaction.extend([c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15, c16, c17, c18, c19, c20])
        transaction.extend([d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22])
        transaction.append(e1)
        transaction.append(z1)
        transaction.extend([g1, g2, g3, g4, g5, g6, g7, g8, g9, g10, g11, g12, g13, g14, g15, g16, g17])
        transaction.extend([h1, h2, h3, h4, h5, h6, h7, h8, h9, h10, h11, h12, h13, h14, h15, h16, h17, h18, h19, h20])
        transaction.extend([i1, i2, i3, i4, i5, i6])
        transaction.extend([j1, j2, j3, j4, j5, j6, j7])
        transaction.extend([k1, k2, k3, k4, k5, k6, k7])
        db["transactions"] = transaction
        db.close()

    except:
        print("Failed to open database")

static_data()
#
#
# list = ["4829,Money Orders - Wire Transfer",
#         "4900,Electric, Gas, Sanitary and Water Utilities",
#         "5013,Motor vehicle supplies ",
#         "5045,Computers, Computer Equipment, Software",
#         "5944,Watch, Clock, Jewelry, and Silverware Stores",
#         "5311,Department Stores",
#         "5411,Supermarkets",
#         "5812,Eating places and Restaurants",
#         "5813,Drinking Places(Bars and Nightclubs)",
#         "5814,Fast Food Restaurants",
#         "5941,Sporting Goods Stores",
#         "5942,Book Stores",
#         "6381,Insurance Premiums",
#         "7277,Counseling Service - Debt, Marriage, Personal",
#         "7997,Membership Clubs",
#         "9311,Tax Payments",
#         "8071,Medical and Dental Laboratories",
#         "1111,Nothing",
#         "5309,Duty Free Store"
#         ]
# try:
#     db = shelve.open('mcc.db', 'c', writeback=True)
#
#     for i in range(len(list)):
#         a = list[i].split(",", 1)
#         db[a[0]] = a[1] + "\n"
#     db.close()
# except:
#     print("Failed to open database")
def get_StrMonth(self):
    if self == 13:
        return "Jan"
    elif self == 2:
        return "Feb"
    elif self == 3:
        return "Mar"
    elif self == 4:
        return "Apr"
    elif self == 5:
        return "May"
    elif self == 6:
        return "Jun"
    elif self == 7:
        return "Jul"
    elif self == 8:
        return "Aug"
    elif self == 9:
        return "Sep"
    elif self == 10:
        return "Oct"
    elif self == 11:
        return "Nov"
    elif self == 12:
        return "Dec"
    else:
        return "Invalid integer"

def get_categoriesmcc(self):
    if self == "Money Orders - Wire Transfer\n":
        return "4829"
    elif self == "Electric, Gas, Sanitary and Water Utilities\n":
        return "4900"
    elif self == "Motor vehicle supplies\n":
        return "5013"
    elif self == "Computers, Computer Equipment, Software\n":
        return "5045"
    elif self == "Watch, Clock, Jewelry, and Silverware Stores\n":
        return "5944"
    elif self == "Department Stores\n":
        return "5311"
    elif self == "Supermarkets\n":
        return "5411"
    elif self == "Eating places and Restaurants\n":
        return "5812"
    elif self == "Drinking Places(Bars and Nightclubs)\n":
        return "5813"
    elif self == "Fast Food Restaurants\n":
        return "5814"
    elif self == "Sporting Goods Stores\n":
        return "5941"
    elif self == "Book Stores\n":
        return "5942"
    elif self == "Insurance Premiums\n":
        return "6381"
    elif self == "Counseling Service - Debt, Marriage, Personal\n":
        return "7277"
    elif self == "Membership Clubs\n":
        return "7997"
    elif self == "Tax Payments\n":
        return "9311"
    elif self == "Medical and Dental Laboratories\n":
        return "8071"
    elif self == "Duty Free Store\n":
        return "5309"

# dict = {}
# b1 = Budget(1, 2000, '8071', 'Nov')
# dict[b1.get_category()] = b1.get_amount()
# db = shelve.open('budget.db', 'c')
# db["Budget"] = dict
# db.close()
